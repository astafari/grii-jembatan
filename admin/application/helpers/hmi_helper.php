<?php
function get_jenis_kendaraan(){
	$CI = getCI();
	$m = $CI->db->get("master_jenis_mobil")->result();
	return $m;
}
function get_customer(){
	$CI = getCI();
	$m = $CI->db->get("master_customer")->result();
	return $m;
}
function get_nama_cust($st){
	$CI = getCI();
	$m = $CI->db->get_where("master_customer",array(
			"customer_id"	=> $st
		))->row();
	return $m;
}
function get_armada(){
	$CI = getCI();
	$m = $CI->db->get("master_armada")->result();
	return $m;
}
function get_nama_armada($st){
	$CI = getCI();
	$m = $CI->db->get_where("master_armada",array(
			"armada_id"	=> $st
		))->row();
	return $m;
}
function get_spec_akun($st){
	$CI = getCI();
	$m = $CI->db->get_where("master_coa",array(
			"coa_parent"	=> $st,
			"cat"			=> "cat"
		))->result();
	return $m;
}
function get_akun(){
	$CI = getCI();
	$m = $CI->db->get("master_jenis_order")->result();
	return $m;
}
function get_jo(){
	$CI = getCI();
	$m = $CI->db->get("master_jenis_order")->result();
	return $m;
}
function get_tujuan(){
	$CI = getCI();
	$m = $CI->db->get("master_tujuan")->result();
	return $m;
}
function get_nama_tujuan($st){
	$CI = getCI();
	$m = $CI->db->get_where("master_tujuan",array(
			"tujuan_id"	=> $st
		))->row();
	return $m;
}
function get_driver(){
	$CI = getCI();
	$m = $CI->db->get("master_driver")->result();
	return $m;
}
function get_nama_driver($st){
	$CI = getCI();
	$m = $CI->db->get_where("master_driver",array(
			"driver_id"	=> $st
		))->row();
	return $m;
}
function get_log($st="",$mt=""){
	$CI = getCI();
	$CI->db->order_by("changelog_date",'desc');
	$m = $CI->db->get_where("app_changelog",array(
			"changelog_menu"	=> $st,
			"changelog_data"	=> $mt
		))->result();
	return $m;
}
function get_order_detail($st){
	$CI = getCI();
	$CI->db->select("trx_order_detail.*, master_jenis_order.jo_name");
	$CI->db->join("master_jenis_order", "master_jenis_order.jo_id=trx_order_detail.jo_id");
	$m = $CI->db->get_where("trx_order_detail",array(
			"order_id"	=> $st
		))->result();
	return $m;
}
function get_order_trx($st){
	$CI = getCI();
	$CI->db->select("trx_jurnal.*, master_coa.coa_name");
	$CI->db->join("master_coa", "master_coa.coa_code=trx_jurnal.jurnal_account");
	$m = $CI->db->get_where("trx_jurnal",array(
			"col1"	=> $st
		))->result();
	return $m;
}
function get_total_amount($st){
	$CI = getCI();
	$CI->db->select("SUM(amount) as amount");
	$m = $CI->db->get_where("trx_order_detail",array(
			"order_id"	=> $st
		))->row();
	return $m;
}
function get_outcome_order($st){
	$CI = getCI();
	$CI->db->select("SUM(jurnal_kredit) as kredit");
	$m = $CI->db->get_where("trx_jurnal",array(
			"col1"	=> $st
		))->row();
	return $m;
}

?>