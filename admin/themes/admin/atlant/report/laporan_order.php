<?php js_validate(); ?>
                <div class="row">			
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
							<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/search" class="form-horizontal" method="post"> 
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-1 col-form-label">Customer</label>
                                                <div class="col-sm-2">
													<select class="form-control" id="exampleFormControlSelect29" name="customer">
														<option value=""> - Pilih Customer - </option>
														<?php foreach((array)get_customer() as $kj=>$vj){
																$s = isset($param)&&$param['customer']==$vj->customer_id?'selected="selected"':'';
																echo "<option value='".$vj->customer_id."' $s >".$vj->customer_name."</option>";
														} ?>
													</select>
												</div>
                                                <label class="col-sm-1 col-form-label">Tgl. Awal</label>
                                                <div class="col-sm-2">
                                                    <input type="date" class="form-control digits" placeholder="Tgl. Awal" name="start_date" value="<?php echo isset($param)?$param['start_date']:date("Y-m-d");?>">
                                                </div>
                                                <label class="col-sm-1 col-form-label">Tgl. Akhir</label>
                                                <div class="col-sm-2">
                                                    <input type="date" class="form-control digits" placeholder="Tgl. Akhir" name="end_date" value="<?php echo isset($param)?$param['end_date']:date("Y-m-d");?>">
                                                </div>
                                                <div class="col-sm-3">
													<button type="submit" class="btn btn-info">Generate</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
							</form>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
				<?php if(isset($param)){ ?>
                <div class="row">			
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Data Order</h5>
                                <span>Daftar Order CV - Anugerah Tanjung Permai.</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive pull-left">
									<table class="table table-striped">
									   <thead align="center">
										<tr>
											<th width="2px">No</th>
											<th width="10px">Tgl. Order</th>
											<th width="10px">Order Code</th>
											<th>Customer</th>
											<th>Tujuan</th>
											<th>Armada / Driver</th>
											<th>Income</th>
											<th>Outcome</th>
											<th>Surplus / Defisit</th>
										</tr>
										</thead>										
									   <tbody align="center">
										<?php
											$no=0; $income=0; $outcome=0; $total=0;
											if(count($data>0)){
												foreach($data as $r){
													$no++;
													$income 	= $income + $r->order_amount;
													$outcome 	= $outcome + get_outcome_order($r->order_id)->kredit;
													$total	 	= $total + ($r->order_amount-get_outcome_order($r->order_id)->kredit);
										?>
											<tr>
												<td width="2px"><?php echo $no;?></td>
												<td width="10px"><?php echo myDate($r->order_date,"d M Y");?></td>
												<td><?php echo $r->order_code;?></td>
												<td><?php echo $r->customer_name;?></td>
												<td><?php echo $r->tujuan;?></td>
												<td><?php echo $r->plat_nomor;?> / <?php echo $r->driver_name;?></td>
												<td>Rp. <?php echo myNum($r->order_amount);?></td>
												<td>Rp. <?php echo myNum(get_outcome_order($r->order_id)->kredit);?></td>
												<td>Rp. <?php echo myNum($r->order_amount-get_outcome_order($r->order_id)->kredit);?></td>
											</tr>
										<?php
												}
											}
										?>
									   </tbody>
									   <tfoot align="center">
										<tr>
											<th width="2px"></th>
											<th width="10px"></th>
											<th width="10px"></th>
											<th></th>
											<th></th>
											<th>TOTAL</th>
											<th><?php echo myNum($income);?></th>
											<th><?php echo myNum($outcome);?></th>
											<th><?php echo myNum($total);?></th>
										</tr>
										</tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
				<?php } ?>
<script type="text/javascript">
	var AUTH_URL = '<?php echo $own_links;?>/ajax_list';
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';
</script>				

				