<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo cfg('app_name');?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        <script type="text/javascript" src="<?php echo themeUrl();?>js/plugins/jquery/jquery.min.js"></script>
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo themeUrl();?>css/theme-default.css"/>
		<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
        <!-- EOF CSS INCLUDE -->        
        <style type="text/css">
        body { color:#000; background:#fff; padding: 0; font-family: 'Merriweather', serif;}
		.content-lg-no-image { 
			background-color:transparent; 
			/*background-size: 100% 100%;
			background-repeat: no-repeat;*/
			height:550px; 
			width: 400px;
		}		
		.content-rapimnas {
			background-image: url('<?php echo base_url()."assets/images/id_card.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-orange {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-DPP.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-kuning {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-DPP-ORMAS.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-hitam {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-Fraksi.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-merah {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-Panitia.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-hijau {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-Peninjau.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-abu {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-TamuPers.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-ungu {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-Technical Staff.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-munaslub-putih {
			background-image: url('<?php echo base_url()."assets/images/ID-Card-WANKARBINHOR.jpg";?>'); 
			background-size: 400px 550px;
			background-repeat: no-repeat; 
			height:550px; 
			width: 400px;
		}
		.content-no-foto {
			background-image: url('<?php echo base_url()."assets/images/id_card.jpg";?>'); 
			background-size: 600px 220px;
			background-repeat: no-repeat; 
			height:220px; 
			width: 350px;
		}
		.content-no-foto {
			background-image: url('<?php echo base_url()."assets/images/id_card.jpg";?>'); 
			background-size: 600px 220px;
			background-repeat: no-repeat; 
			height:220px; 
			width: 350px;
		}
		.nama,.nomor,.domisili,.masa{
			text-align: center;
		}
		.nama{
			font-size: 17px;
			font-weight: bold;
			line-height: 35px;
			color:black;
		}
		.judul{
			font-size: 16px;
			line-height: 25px;
			color:black;
		}
		.kelompok{
			font-size: 16px;
			font-weight: bold;
			line-height: 25px;
			color:black;
		}
		.nomor{
			font-size: 12px;
			line-height: 12px;
			color:black;
		}
		.domisili{
			font-size: 25px;
			line-height: 15px;
			color:black;
		}
		.masa{
			font-size: 7px;
			margin-top: 5px;
		}
        @media print {
        	.hidden-print {
        		display: none !important;
        	}
        }
		</style> 
    </head>
    <body>
		<div class="panel-body" style="padding: 0;">
			<input type="hidden" id="id" value="<?php echo $id;?>">         
			<div class="row">
							<div id="canvas-card" class="canvas"> 
                                	<div class="content-lg-no-image">
										<div style="float:right;margin:225px 0px 0 0;">&nbsp;</div>
										<div style="clear:both;margin:0px 0px 0px 0;" align="center">
										<img alt="" src="<?php echo get_image(base_url()."assets/collections/kta/photo/".$foto);?>" style="height:100px; width:70px;" >
										</div>								
										<div style="clear:both;margin:0px 0px 0px 0;" align="center">
											<div class="nama" align="center"> <?php echo strtoupper($nama);?> </div>
											<div  style="clear:both;margin:0px 0px 0px 35px;" align="center">
											<table width="80%" align="center">
												<tr>
													<td class ="judul" width="130px">NO. PESERTA</td>
													<td class ="judul" width="10">:</td>
													<td class ="kelompok"> <?php echo strtoupper($no_urut);?> </td>
												</tr>
												<tr>
													<td class ="judul" width="130px">KELOMPOK</td>
													<td class ="judul" width="10">:</td>
													<td class ="kelompok"> <?php echo strtoupper($dapil);?> </td>
												</tr>
											</table>
											</div>								
										</div>       
									</div>
							</div>
						<br>
				</div>

				</div>
			</div><br />
			<div class="row hidden-print" style="padding: 0 20px 10px;">
				<div class="form-group">
					<button class="btn btn-success pull-left" id="btn_login"> Print </button>
					<button class="btn btn-danger pull-right" onclick="window.close()"> Cancel </button>
				</div>
			</div>
		</div>        
    </body>

<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/base64.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/canvas2image.js"></script>
<script type="text/javascript">
var URL_UPDATE = '<?php echo $url;?>';
$(document).ready(function(){
	$('#canvas-card').html2canvas({ 
		onrendered: function (canvas) {  
			var img = canvas.toDataURL("image/png");
			$('#canvas-card').html('<img alt="" src="'+img+'" style="height:auto;width:100%px;" >');			 
		}
	});

	$('#btn_login').click(function(){
		$.post(URL_UPDATE,{id:$("#id").val()},function(o){
			window.print(); 
			window.close();
		});
	});
});	
</script>        
</html>