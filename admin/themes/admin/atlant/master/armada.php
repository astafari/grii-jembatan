<?php js_validate(); ?>
                <div class="row">			
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Data Cabang GRII</h5>
                                <span>Daftar Cabang GRII di Indonesia</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive pull-left">
									<table id="table-armada" class="display">
									   <thead align="center">
										<tr>
											<th width="2px">No</th>
											<th>Provinsi</th>
											<th>Kota</th>
											<th>Cabang</th>
											<th>Alamat Cabang</th>
											<th>Status</th>
											<th width="50px">Action</th>
										</tr>
										</thead>
									   <tbody align="center">
									   </tbody>
									   <tfoot align="center">
										<tr>
											<th width="10px">No</th>
											<th>Provinsi</th>
											<th>Kota</th>
											<th>Cabang</th>
											<th>Alamat Cabang</th>
											<th>Status</th>
											<th width="50px">Action</th>
										</tr>
										</tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
<script type="text/javascript">
	var AUTH_URL = '<?php echo $own_links;?>/ajax_list';
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';
</script>				

				