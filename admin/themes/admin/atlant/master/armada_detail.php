                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Data Cabang GRII</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Jenis Kendaraan</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" id="exampleFormControlSelect29" name="jm" ReadOnly>
													<?php foreach ((array)get_jenis_kendaraan() as $m) {
														$s = isset($val)?($val->jm_id==$m->jm_id?'selected="selected"':''):'';
														echo "<option value='".$m->jm_id."' $s >".$m->jm_name."</option>";
													}?>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Plat Nomor</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly class="form-control" placeholder="Masukan Plat Nomor Kendaraan" name="plat" value="<?php echo isset($val)?$val->plat_nomor:"";?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Kondisi Kendaraan Saat Ini</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" readonly id="exampleFormControlTextarea4" rows="3" name="keterangan"><?php echo isset($val)?$val->keterangan:"";?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Status Kendaraan</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" readonly id="exampleFormControlSelect29" name="status">
                                                    <?php foreach((array)cfg('status-armada') as $kj=>$vj){
															$s = isset($val)&&$val->status==$kj?'selected="selected"':'';
															echo "<option value='".$kj."' $s >".$vj."</option>";
													} ?>
                                                </select>
                                                </div>
                                            </div>												
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
				</div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>History - Kondisi Kendaraan</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive pull-left">
											<table id="table-history-armada" class="display">
											   <thead align="center">
												<tr>
													<th width="2px">No</th>
													<th width="10px">Tgl</th>
													<th>Keterangan</th>
													<th width="10px">Status</th>
													<th>User</th>
												</tr>
												</thead>
											   <tbody align="center">
												<?php 
													$status = cfg('status-armada');
													$no=0; 
													foreach ((array)get_log("master_armada",$val->armada_id) as $m) { 
													  $no++;
													  $data = json_decode($m->changelog_request);
												?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $m->changelog_date;?></td>
													<td><?php echo $data->keterangan;?></td>
													<td><?php echo $status[$data->status];?></td>
													<td><?php echo $m->changelog_user;?></td>
												</tr>
												<?php }?>											   
											   </tbody>
											   <tfoot align="center">
												<tr>
													<th width="2px">No</th>
													<th width="10px">Tgl</th>
													<th>Keterangan</th>
													<th width="10px">Status</th>
													<th>User</th>
												</tr>
												</tfoot>
											</table>
										</div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>History - Order Kendaraan Plat Nomor : <?php echo $val->plat_nomor;?></h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive pull-left">
											<table id="table-order-armada" class="display">
											   <thead align="center">
												<tr>
													<th width="2px">No</th>
													<th width="10px">Tgl</th>
													<th>Kode Order - Tujuan</th>
													<th>Supir</th>
												</tr>
												</thead>
											   <tbody align="center">
											   </tbody>
											   <tfoot align="center">
												<tr>
													<th width="2px">No</th>
													<th width="10px">Tgl</th>
													<th>Kode Order - Tujuan</th>
													<th>Supir</th>
												</tr>
												</tfoot>
											</table>
										</div>
                                    </div>
                                </div>
                        </div>
                    </div>
				</div>				