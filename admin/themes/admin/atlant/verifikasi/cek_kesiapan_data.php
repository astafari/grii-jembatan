<form action="<?php echo $own_links;?>/search" method="post" id="form-validated" >
	  <div class="well">
	  		<div class="row">
	  			<div class="col-md-3">		  			
					<select class="validate[required] form-control select" id="propinsi" name="propinsi" data-live-search="true">
						<option value=""> - provinsi - </option>
						<?php foreach ((array)get_propinsi() as $m) {
							$s = isset($param['propinsi'])?($param['propinsi']==$m->propinsi_kode?'selected="selected"':''):'';
							echo "<option value='".$m->propinsi_kode."' $s >".$m->propinsi_nama."</option>";
						}?>
					</select>
		  		</div>
		  		
	  			<div class="col-md-3">
					<select class="validate[required] form-control" id="kabupaten" name="kabupaten" data-live-search="true">
						<option value=""> - kabupaten/kota - </option>
					</select>
	  			</div>
		  		
	  			<div class="col-md-3">
					<select class="validate[required] form-control" id="kecamatan" name="kecamatan" data-live-search="true">
						<option value=""> - kecamatan - </option>
					</select>
	  			</div>
		  		
	  			<div class="col-md-3">
					<select class="form-control" id="kelurahan" name="kelurahan" data-live-search="true">
						<option value=""> - kelurahan - </option>
					</select>
	  			</div>		  		
	  		</div><br />
			<br>
	  		<div class="row">
				<div class="col-md-3" style="margin-top:0px;">
		  			<input type="submit" value="Search!" style="margin-right:5px;" name="btn_search" id="btn_search"  class="btn btn-primary col-md-5" />
		  			<input type="submit" value="Reset!" name="btn_reset" id="btn_reset" class="btn btn-warning col-md-5" />	
	  			</div>
	  		</div>	  	
	  </div>
</form>	  
<?php if(isset($param)){ ?>
<div class="panel panel-default" style="margin-top:-10px;">
    <div class="panel-body panel-body-table">
        <div class="table-responsive">
            <table class="table datatable">
               <thead>
                <tr>
                    <th width="30px">No</th>
                    <th width="30px">NIK</th>
                    <th width="100px">NPAPG</th>
                    <th width="100px">Nama Lengkap</th>
                    <th width="100px">Tempat Lahir</th>
                    <th width="100px">Tanggal Lahir</th>
                    <th width="50px">Status</th>
                    <th width="50px">Keterangan</th>
                </tr>
                </thead>
               <tbody> 
                <?php 
                if(count($data) > 0){
					$no=1;
                    foreach($data as $r){
						if($r->kta_tipe_kta == "SUKET"){
							$sipol = "0";
							$desc  = "DATA SUKET";
							$bgcolor = "#f60404";
						}elseif(strlen($r->kta_no_id) > 16 || strlen($r->kta_no_id) < 16){
							$sipol = "0";
							$desc  = "NIK TIDAK SESUAI";
							$bgcolor = "#f60404";
						}elseif(empty($r->kta_alamat)){
							$sipol = "0";
							$desc  = "TIDAK ADA ALAMAT";
							$bgcolor = "#f60404";
						}else{
							$sipol = "1";
							$desc  = "DATA SIAP DIUPLOAD";							
							$bgcolor = "#fff";
						}
                        ?>
                        <tr style="background-color:<?php echo $bgcolor;?>">
                            <td><?php echo $no++;?></td>
                            <td><?php echo $r->kta_no_id;?></td>
                            <td><?php echo $r->kta_nomor_kartu;?></td>
                            <td><?php echo $r->kta_nama_lengkap;?></td>
                            <td><?php echo $r->kta_tempat_lahir;?></td>
                            <td><?php echo $r->kta_tgl_lahir;?></td>
                            <td>
							<?php if($sipol == 0){
									echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="'.$desc.'"><li class="fa fa fa-spinner"></li> Suspend</span>';								
								  }else{
									echo '<span class="label label-success" data-toggle="tooltip" data-placement="top" title data-original-title="'.$desc.'"><li class="fa fa-check"></li> Ready</span>';								
								  }
							?></td>
                            <td><?php echo $desc;?></td>
                        </tr>
                <?php } 
                }
                ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>
<?php } ?>
<script type="text/javascript">
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';

    $(document).ready(function(){
	<?php if(isset($param) && !empty($param['propinsi'])){?>
		get_kabupaten('<?php echo $param['propinsi'];?>','<?php echo isset($param['kabupaten'])?$param['kabupaten']:'';?>');
	<?php }
		if(isset($param) && !empty($param['kabupaten'])){?>
		get_kecamatan('<?php echo $param['kabupaten'];?>','<?php echo isset($param['kecamatan'])?$param['kecamatan']:'';?>');
	<?php }
		if(isset($param) && !empty($param['kecamatan'])){?>
		get_kelurahan('<?php echo $param['kecamatan'];?>','<?php echo isset($param['kelurahan'])?$param['kelurahan']:'';?>');
	<?php } ?>
    
      $('#propinsi').change(function(){
          get_kabupaten($(this).val(),"");
      });

      $('#kabupaten').change(function(){
          get_kecamatan($(this).val(),"");
      });
	  
      $('#kecamatan').change(function(){
          get_kelurahan($(this).val(),"");
      });
		  
    });

	  function get_kabupaten(prov,kab){
      $.post(URL_AJAX+"/kabupaten",{prov:prov,kab:kab},function(o){
        $('#kabupaten').html(o);
      });
    }

    function get_kecamatan(prov,kab){
      $.post(URL_AJAX+"/kecamatan",{prov:prov,kab:kab},function(o){
        $('#kecamatan').html(o);
      });
    }
    function get_kelurahan(prov,kab){
      $.post(URL_AJAX+"/kelurahan",{prov:prov,kab:kab},function(o){
        $('#kelurahan').html(o);
      });
    }
</script>

    