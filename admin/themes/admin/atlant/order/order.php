<?php js_validate(); ?>
                <div class="row">			
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Data Order</h5>
                                <span>Daftar Order CV - Anugerah Tanjung Permai.</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive pull-left">
									<table id="table-order" class="display">
									   <thead align="center">
										<tr>
											<th width="2px">No</th>
											<th width="10px">Tgl. Order</th>
											<th width="10px">Order Code</th>
											<th>Customer</th>
											<th>Tujuan</th>
											<th>Armada / Driver</th>
											<th>Biaya</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
										</thead>
									   <tbody align="center">
									   </tbody>
									   <tfoot align="center">
										<tr>
											<th width="2px">No</th>
											<th width="10px">Tgl. Order</th>
											<th width="10px">Order Code</th>
											<th>Customer</th>
											<th>Tujuan</th>
											<th>Armada / Driver</th>
											<th>Biaya</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
										</tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
<script type="text/javascript">
	var AUTH_URL = '<?php echo $own_links;?>/ajax_list';
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';
</script>				

				