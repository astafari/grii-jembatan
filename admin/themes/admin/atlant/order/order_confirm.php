                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Halaman Konfirmasi - Order</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tbody>
												<tr>
													<td width="150px">Tgl. Order</td>
													<td width="2px">:</td>
													<td align="left"><?php echo myDate($val->order_date,"d M Y");?></td>
												</tr>
												<tr>
													<td>Customer</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_cust($val->order_customer)->customer_name;?></td>
												</tr>
												<tr>
													<td width="150px">Tgl. Pick Up</td>
													<td width="2px">:</td>
													<td align="left"><?php echo myDate($val->order_pick_up,"d M Y");?></td>
												</tr>
												<tr>
													<td>Armada / Supir</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_armada($val->order_armada)->plat_nomor;?> /  <?php echo get_nama_driver($val->order_driver)->driver_name;?></td>
												</tr>
												<tr>
													<td>Tujuan</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_tujuan($val->order_destination)->tujuan;?></td>
												</tr>
												<tr>
													<td>No. Container</td>
													<td>:</td>
													<td align="left"><?php echo $val->order_container_number;?></td>
												</tr>
												</tbody>
											</table>
										</div>									
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Biaya Order</h5>
                            </div>
                                <div class="card-body">
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
												<tbody>
												<?php $no=1; $total=0; foreach((array)get_order_detail($val->order_id) as $kj=>$vj){ $no++;
													  $total = $total + $vj->amount;
												?>
													<tr>
														<td width="300px" align="left"><?php echo $vj->jo_name;?> <?php echo $val->order_day;?> hari</td>
														<td width="2px">:</td>
														<td align="left">Rp. <?php echo myNum($vj->amount);?></td>
													</tr>
												<?php } ?>
													<tr>
														<td width="300px" align="left">Total Biaya</td>
														<td width="2px">:</td>
														<td align="left"><strong>Rp. <?php echo myNum($total);?></strong></td>
													</tr>
												</tbody>
											</table>
										</div>											
                                    </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <a href="<?php echo $own_links."/edit?_id="._encrypt($val->order_id);?>" class="btn btn-info">Edit Order</a>
                                        <a href="<?php echo $own_links."/proceed?_id="._encrypt($val->order_id);?>" class="btn btn-success">Submit Order</a>
                                    </div>
                                </div>									
                                </div>
                        </div>
                    </div>
				</div>				