        <script type="text/javascript" src="<?php echo themeUrl();?>js/highmaps.js"></script>        
        <script type="text/javascript" src="<?php echo themeUrl();?>js/id-all.js"></script>        
			<?php js_hight_chart();
			foreach ((array)get_cart_kab($id_prop) as $p => $q) {
				$koma = $p>0?',':'';
				$jumlah 		 = $jumlah.$koma."['".$q->kab_nama."',".$q->jumlah."]";
			}			
			?>
		<script type="text/javascript">
			$(function () {
				// Initiate the chart
				$('#container').highcharts('Map', {
					 tooltip: {
						 formatter: function(){
							 var s = this.key + '<br/>';
							 s += 'Jumlah Anggota : ' + this.point.value+ '<br/>';
							 return s;
						 },
					 },
			
					legend: {
						enabled: false
					  },
					title : {
						text : 'Peta Distribusi'
					},
					subtitle : {
						text : 'Anggota Partai Golkar'
					},

					mapNavigation: {
						enabled: true,
						buttonOptions: {
							verticalAlign: 'top'
						}
					},
						colorAxis: {
							min: 0,
							stops: [
								[0, '#dce8df'],
								[0.5, Highcharts.getOptions().colors[0]],
								[1, Highcharts.Color(Highcharts.getOptions().colors[0]).brighten(-0.5).get()]
							]
						},
					series:
					[				
					  {
						color: '#efef02',
						states: {
							select: {
								color: '#a4edba',
								borderColor: 'black',
								dashStyle: 'shortdot'
									}
								},
						dataLabels: {
							enabled: true,
							format: '{point.name}'
						},
						"name": "PROVINSI",
						"type": "map",
						"data": [
								<?php 
								foreach ((array)get_path($id_prop) as $k => $v) {
									$koma = $k<count(get_path($id_prop))-1?",":"";
									echo "{";
									echo '"name":"'.$v->kab_nama.'",';				
									echo '"path":"'.$v->path.'",';				
									echo '"value":'.get_path_value($v->kab_kode);				
									echo "}".$koma;				
								}?>						

						]
					  }
					]
				});
			});
		</script>
                            
    <form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/detail" class="form-horizontal" method="post"> 
        <input type="hidden" name="kta_id" id="kta_id" value="<?php echo isset($val->kta_id)?$val->kta_id:'';?>" />

        <div class="panel-body">                                                                        
            <div class="row">
             <div class="col-md-4"  align="center">
					<div id ="container"></div>
			 </div>                
                <div class="col-md-8">
                    <h3>INFORMASI KEANGGOTAAN GOLKAR PROVINSI <?php echo $prop_nama;?></h3>
					<div class="panel" style="height:370px;padding:10px;" id="cart_bar_by_prov">
					</div>
                </div>
			 </div>
            <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default" style="margin-top:-10px;">
        <div class="panel-body panel-body-table">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                   <tbody> 
                    <tr>
                        <td><b>Kode Provinsi <?php echo $prop_nama;?></b></td>
                        <td align="right"><b><?php echo $id_prop;?></b></td>
                        <td><b>Jumlah Kabupaten</b></td>
                        <td align="right"><b><?php echo myNum(get_sum_kab($id_prop));?></b></td>
                        <td><b>Total Anggota</b></td>
                        <td align="right"><b><b><?php echo myNum(get_total_by_prop($id_prop));?></b></td>
                        <td><b>Total Pengusul</b></td>
                        <td align="right"><b><?php echo myNum(get_sum_pengusul($id_prop));?></b></td>
                    </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
		
    </div>
	</div>
			 </div>                            			 
            <div class="row">
             <div class="col-md-4">
			 </div>                
                <div class="col-md-8">
                </div>
			 </div>
            <div class="row">
                <div class="col-md-5">
						<div class="panel" style="height:370px;padding:10px;" id="cart_line_anggota">            
					</div>
                </div>
				<div class="col-md-7">
				<div class="panel panel-default" style="height:370px;padding:10px;">
					<div class="panel-heading">
						<div class="panel-title-box">
							<h3>Tabel Progress Data Berdasarkan Jenis Kelamin</h3>
							<span>Provinsi <?php echo $prop_nama;?></span>
						</div>                                    
						<ul class="panel-controls" style="margin-top: 2px;">
							<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
						</ul>
					</div>
					<div class="panel-body panel-body-table scroll" style="height: 300px; overflow-y:">
							<div class="table-responsive">
							<table class="table table-hover table-bordered table-striped">
							   <thead>
								<tr>
									<th>Nama Kabupaten</th>
									<th>Perempuan</th>
									<th>Laki-laki</th>
									<th>Total</th>
								</tr>
								<?php
									foreach ((array)get_progress_jk($id_prop) as $p => $q) {
										$jumlahjk 		= $q->laki + $q->perempuan;
										$totaljk		= $totaljk + $jumlahjk;
										$totalLaki		= $totalLaki + $q->laki;
										$totalPer		= $totalPer + $q->perempuan;
										$totalPercent 	= ($totaljk / (get_sum_kab($id_prop) * 1500)) * 100; 
										$totalLakiPercent = ($totalLaki / $totaljk) * 100;
										$totalPerPercent  = ($totalPer / $totaljk) * 100;
										$jumlahPercent  = ($jumlahjk / 1500) * 100;
										$LakiPercent 	= ($q->laki / $jumlahjk ) * 100; 
										$PerPercent 	= ($q->perempuan / $jumlahjk ) * 100; 
								?>
								<tr>
									<td><?php echo $q->kab_nama;?></td>
									<td>
												<?php echo $q->laki." (".round($LakiPercent,2)." %)"; ?>
									</td>
									<td>
												<?php echo $q->perempuan." (".round($PerPercent,2)." %)"; ?>
									</td>
									<td>
												<?php echo $jumlahjk." dari 1500 (".round($jumlahPercent,2)." %)"; ?>
									</td>
								</tr>
								  <?php } ?>
								<tr style="background-color:#ccc;">
									<td><b>TOTAL</b></td>
									<td><b>
												<?php echo $totalLaki." (".round($totalLakiPercent,2)." %)"; ?>
									</td></b>
									<td><b>
												<?php echo $totalPer." (".round($totalPerPercent,2)." %)"; ?>
									</td></b>
									<td><b>
												<?php echo $totaljk." dari ".(get_sum_kab($id_prop) * 1500)." (".round($totalPercent,2)." %)"; ?>
									</td></b>
								</tr>
								</thead>
							   <tbody> 
								</tbody>
							</table>
							
						</div>
					</div>
					
				</div>
				</div>
                </div>
			 </div>   			 
        </div>
        </div>
    </form>
<script type="text/javascript">

$(function () {
    //line pertumbuhan anggota
    $('#cart_line_anggota').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>Grafik Upload Data Anggota Provinsi <?php echo $prop_nama;?></b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Anggota',
            data: [
			<?php
              foreach ((array)get_anggota_baru($id_prop) as $p => $q) {
                echo $p>0?',':'';
                echo "['".$q->tgl."',".$q->jumlah."]";
              }
            ?>
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#111',
                align: 'right',
                x: 4,
                y: -20,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#009F9A'
        }
		]
    });
    $('#cart_line_order').highcharts({
chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Browser market shares January, 2015 to May, 2015'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Microsoft Internet Explorer',
                y: 56.33
            }, {
                name: 'Chrome',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: 'Firefox',
                y: 10.38
            }, {
                name: 'Safari',
                y: 4.77
            }, {
                name: 'Opera',
                y: 0.91
            }, {
                name: 'Proprietary or Undetectable',
                y: 0.2
            }]
        }]
    });

    $('#cart_bar_by_prov').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>Jumlah Anggota Per Kabupaten / Kota Provinsi <?php echo $prop_nama;?></b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Anggota',
            data: [<?php echo $jumlah;?>],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#111',
                align: 'right',
                x: 4,
                y: -20,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#009F9A'
        }					
		]
    });
});

</script>
                            