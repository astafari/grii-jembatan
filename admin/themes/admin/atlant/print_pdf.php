<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo cfg('app_name');?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        <script type="text/javascript" src="<?php echo themeUrl();?>js/plugins/jquery/jquery.min.js"></script>
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo themeUrl();?>css/theme-default.css" />
        <!-- EOF CSS INCLUDE -->        
        <style type="text/css">
        body { color:#000; background:#fff; padding: 0;}
        .content-lg { 
			background-image: url('<?php echo base_url()."assets/images/id_card_front.jpg";?>'); 
			background-size: 100% 100%;
			background-repeat: no-repeat;
			height:650px; 
			width:1063px;
		}
		.content-lg-no-image { 
			background-color:transparent; 
			/*background-size: 100% 100%;
			background-repeat: no-repeat;*/
			height:650px; 
			width:1063px;
		}
		.nama-lg,.nomor-lg,.domisili-lg,.masa-lg{
			text-align: right;
		}
		.nama-lg{
			font-size: 50px;
			font-weight: bold;
			line-height: 50px;
		}
		.nomor-lg{
			font-size: 35px;
			line-height: 50px;
		}
		.domisili-lg{
			font-size: 30px;
			line-height: 28px;
		}
		.masa-lg{
			font-size: 18px;
			margin-top: 30px;
		}
		@media print {
		tr {
			-webkit-print-color-adjust: exact; 
		}}
		</style> 
    </head>
	<body>
		<div class="panel-body" style="padding: 0;">
			<input type="hidden" id="id" value="<?php echo $id;?>">         
			<div class="row">
                <div class="col-md-12">
					<iframe id="fred" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="<?php echo base_url().'assets/report/pdf/'.$filename;?>" frameborder="1" scrolling="auto" height="1100" width="850" ></iframe>				
				</div>
			</div>
			</div><br />
		</div>        
    </body>

<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/base64.js"></script>
<script type="text/javascript" src="<?php echo themeUrl();?>js/html2canvas/canvas2image.js"></script>
<script type="text/javascript">
</script>        
</html>