<?php
class mdl_report extends CI_Model{ 
		
	function __construct(){
		parent::__construct();
		///this->db = $this->load->database('rdb', TRUE);
	} 
	
	function order_list($p=array(),$count=FALSE){
		
		$total = 0;

		$this->db->select('trx_order.*, master_armada.plat_nomor, master_driver.driver_name, master_tujuan.tujuan, master_customer.customer_name');		
		$this->db->join('master_armada','master_armada.armada_id=trx_order.order_armada','LEFT');
		$this->db->join('master_driver','master_driver.driver_id=trx_order.order_driver','LEFT');
		$this->db->join('master_tujuan','master_tujuan.tujuan_id=trx_order.order_destination','LEFT');
		$this->db->join('master_customer','master_customer.customer_id=trx_order.order_customer','LEFT');
		if(isset($p['customer']) ){
			if($p['customer'] != ""){
			$this->db->where("order_customer",$p['customer']);
			}
		}
		/* where or like conditions */
		$start_time = $p['start_date'];
		$end_time = $p['end_date'];

		if( trim($p['start_date'])!="" && trim($p['end_date']) != "" ){
			$this->db->where("( order_date >= '".$start_time."' AND order_date <= '".$end_time."' )");
		}


		if($count==FALSE){
			if( isset($p['offset']) && isset($p['limit']) ){
				$p['offset'] = empty($p['offset'])?0:$p['offset'];
				$this->db->limit($p['limit'],$p['offset']);
			}
		}
		$this->db->order_by('order_date','DESC');
		//if(trim($this->jCfg['search']['order_by'])!="" && !isset($p['id']) )
			//$this->db->order_by($this->jCfg['search']['order_by'],$this->jCfg['search']['order_dir']);
		
		$qry = $this->db->get('trx_order');
		if($count==FALSE){
			$total = $this->order_list($p,TRUE);
			return array(
					"data"	=> $qry->result(),
					"total" => $total
				);
		}else{
			return $qry->num_rows();
		}
		

	}
}