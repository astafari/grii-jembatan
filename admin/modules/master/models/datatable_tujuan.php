<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Datatable_tujuan extends CI_Model {
 
    var $table = 'master_tujuan';
    var $column_order = array(null,'date','pendeta','seri','judul','keterangan','cabang','link_1','reformed_link','zoom_link','link_2','status'); //set column field database for datatable orderable
    var $column_search = array('date','pendeta','seri','judul','keterangan','cabang','link_1','reformed_link','zoom_link','link_2'); //set column field database for datatable searchable 
    var $order = array('tujuan_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
    }
 
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
        $i = 0;
		
			$str_like = "( ";
			$i=0;
			foreach ($this->column_search as $item){ // loop column 
					$str_like .= $i!=0?"OR":"";
					$str_like .=" ".$item." LIKE '%".$_POST['search']['value']."%' ";			
					$i++;
			}
			$str_like .= " ) ";
			$this->db->where($str_like);     
//			$this->db->limit(100);
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}