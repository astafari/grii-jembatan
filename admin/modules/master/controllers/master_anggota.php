<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Master_anggota extends AdminController {  
    function __construct()    
    {
        parent::__construct(); 
        error_reporting(E_ALL);
        $this->_set_action();
        $this->_set_action(array("edit","delete"),"ITEM"); //"view"
        $this->_set_title('Data DPT 2019');
        $this->DATA->table="app_kta";
        $this->folder_view = "master/";
        $this->prefix_view = strtolower($this->_getClass());
        $this->breadcrumb[] = array(
                "title"     => "Master Anggota",
                "url"       => $this->own_link
            );

		$this->upload_path="./assets/collections/kta/";
    
		if(!isset($this->jCfg['search']['class']) || $this->jCfg['search']['class'] != $this->_getClass()){
            $this->_reset();
//            redirect($this->own_link);
        }
        
    
		$this->cat_search = array(
			''					=> 'All',
			'kta_nomor_kartu'	=> 'NPAPG',
			'kta_nama_lengkap'	=> 'Nama Lengkap',
			'kta_tempat_lahir'	=> 'Tempat Lahir',
			'propinsi_nama'		=> 'Propinsi',
			'kab_nama'			=> 'Kabupaten',
			'kec_nama'			=> 'Kecamatan',
			'kel_nama'			=> 'Kelurahan',			
		); 
        $this->load->model("mdl_master","M");
 		$this->load->model("datatable_master","D");
		$this->css_file = array(
			'cropper/v0.7.9/css/cropper.min.css',
			'cropper/v0.7.9/css/main.css'
//			'cropper/cropper.min.css'
		);
		
		//load js..
		$this->js_plugins = array(
			'plugins/bootstrap/bootstrap-datepicker.js',
			'plugins/bootstrap/bootstrap-file-input.js',
			'plugins/bootstrap/bootstrap-select.js',
			'plugins/webcamjs/webcam.js',
			'plugins/fileinput/fileinput.min.js',
			'plugins/datatables/jquery.dataTables.min.js',
//			'plugins/blueimp/jquery.blueimp-gallery.min.js',
			'cropper/docs/v0.7.9/js/cropper.min.js'
//			'cropper/docs/v0.7.9/js/main2.js'
//			'plugins/dropzone/dropzone.min.js',
//			'plugins/filetree/jqueryFileTree.js',
//			'plugins/jstree/jstree.min.js',
//			'plugins/cropper/cropper.min.js'
		);
		
		$this->js_file = array(
			'cropper/docs/v0.7.9/js/main.js'
		);
		}

    
    function _reset(){		
		$this->jCfg['search'] = $this->sCfg['search'] = array(
								'class'		=> $this->_getClass(),
								'per_page'	=> 10,
								'result'	=> '',
								'propinsi'	=> '',
								'kabupaten'	=> '',
								'kecamatan'	=> '',
								'kelurahan'	=> '',
								'pengusul'	=> '',
								'nama'		=> '',
								'status'	=> '',
								'angkatan'	=> '',
								'unsur'		=> '',
								'nik'		=> '',
								'npapg'		=> '',
								'usulan'	=> ''
							);
							
        $this->jCfg['page_tab'] = $this->sCfg['page_tab'] = '1';
        $this->jCfg['type_data'] = $this->sCfg['type_data'] = 1;
        $this->_releaseSession();
    }

    function set_tab(){
        $tab = $this->input->get('tab');
        $this->sCfg['type_data'] = $tab;
        $this->_releaseSession();

        $next = $this->own_link;
        if(isset($_GET['next'])){
            $next = $_GET['next'];
        }

        redirect($next);
    }
    
    function index() {

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		
		$data = array('data' => array());
		if($this->input->post('btn_search')){			
			if($this->input->post('propinsi') && trim($this->input->post('propinsi'))!="")
				$this->jCfg['search']['propinsi'] = $this->sCfg['search']['propinsi'] = $this->input->post('propinsi');
			else
				$this->jCfg['search']['propinsi'] = $this->sCfg['search']['propinsi'] = "";

			if($this->input->post('kabupaten') && trim($this->input->post('kabupaten'))!="")
				$this->jCfg['search']['kabupaten'] = $this->sCfg['search']['kabupaten'] = $this->input->post('kabupaten');
			else
				$this->jCfg['search']['kabupaten'] = $this->sCfg['search']['kabupaten'] = "";

			if($this->input->post('angkatan') && trim($this->input->post('angkatan'))!="")
				$this->jCfg['search']['angkatan'] = $this->sCfg['search']['angkatan'] = $this->input->post('angkatan');
			else
				$this->jCfg['search']['angkatan'] = $this->sCfg['search']['angkatan'] = "";	

			if($this->input->post('usulan') && trim($this->input->post('usulan'))!="")
				$this->jCfg['search']['usulan'] = $this->sCfg['search']['usulan'] = $this->input->post('usulan');
			else
				$this->jCfg['search']['usulan'] = $this->sCfg['search']['usulan'] = "";	

			if($this->input->post('unsur') && trim($this->input->post('unsur'))!="")
				$this->jCfg['search']['unsur'] = $this->sCfg['search']['unsur'] = $this->input->post('unsur');
			else
				$this->jCfg['search']['unsur'] = $this->sCfg['search']['unsur'] = "";
				
			if($this->input->post('npapg') && trim($this->input->post('npapg'))!="")
				$this->jCfg['search']['npapg'] = $this->sCfg['search']['npapg'] = $this->input->post('npapg');
			else
				$this->jCfg['search']['npapg'] = $this->sCfg['search']['npapg'] = "";

			if($this->input->post('nik') && trim($this->input->post('nik'))!="")
				$this->jCfg['search']['nik'] = $this->sCfg['search']['nik'] = $this->input->post('nik');
			else
				$this->jCfg['search']['nik'] = $this->sCfg['search']['nik'] = "";

			$this->jCfg['search']['result'] = 1;
			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}
			$data = $this->_data(array(
					"base_url"	=> $this->own_link.'/index/'
				));			
		$this->_v($this->folder_view.$this->prefix_view,$data);
		
    }
	
	function add(){	
		$this->breadcrumb[] = array(
				"title"		=> "Add"
			);		
		$this->_v($this->folder_view.$this->prefix_view."_form",array());
	}
    function edit(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'kta_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_form",array());
		}else{
			redirect($this->own_link);
		}

    }
    
    function delete(){
        $id=_decrypt(dbClean(trim($this->input->get('_id'))));
        if(trim($id) != ''){
            $o = $this->DATA->_delete(
                array("kta_id"  => idClean($id)),
                TRUE
            );
            
        }
        redirect($this->own_link."?msg=".urldecode('Delete data KTA succes')."&type_msg=success");
    }
    
    function update(){
        $id=$this->input->post('id');
        if(trim($id) != ''){
            $o = $this->DATA->_update(
            	array("kta_id"  => idClean($id)),
            	array("is_cetak"  		 => 1,
					  "time_print_card"  => date("Y-m-d H:i:s"),
					  "col14"  			 => $this->jCfg['user']['id'],
					  )
            );
            
            echo $o;
        }
    }
    function ajax_list()
    {
        $list = $this->D->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {	
			$tahun_lahir = explode("|",$customers->kta_tgl_lahir);
			$umur		 = date("Y")-$tahun_lahir[2];
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->kta_nkk;
            $row[] = $customers->kta_nik;
            $row[] = $customers->kta_nama_lengkap;
            $row[] = $customers->kta_tempat_lahir.", ".$customers->kta_tgl_lahir;
            $row[] = $umur;
            $row[] = $customers->kta_jenkel;
            $row[] = $customers->kta_difabel;
            $row[] = $customers->kta_tps;
            $row[] = $customers->kta_alamat." RT ".$customers->kta_rt." /RW ".$customers->kta_rw;
            $row[] = $customers->kel_nama;
            $row[] = $customers->kel_nama;
            $row[] = $customers->kel_nama;
            $row[] = $customers->kel_nama;
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->D->count_all(),
                        "recordsFiltered" => $this->D->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }	
}