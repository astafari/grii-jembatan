<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Order extends AdminController {  
    function __construct()    
    {
        parent::__construct(); 
        error_reporting(E_ALL);
        $this->_set_action();
        $this->_set_action(array("edit","delete"),"ITEM"); //"view"
        $this->_set_title('Data Order CV - ATP');
        $this->DATA->table="trx_order";
        $this->folder_view = "order/";
        $this->prefix_view = strtolower($this->_getClass());
        $this->breadcrumb[] = array(
                "title"     => "Data Order CV-ATP",
                "url"       => $this->own_link
            );

		$this->upload_path="./assets/collections/kta/";
    
		if(!isset($this->jCfg['search']['class']) || $this->jCfg['search']['class'] != $this->_getClass()){
            $this->_reset();
//            redirect($this->own_link);
        }
        
    
		$this->cat_search = array(
			''					=> 'All',
			'kta_nomor_kartu'	=> 'NPAPG',
			'kta_nama_lengkap'	=> 'Nama Lengkap',
			'kta_tempat_lahir'	=> 'Tempat Lahir',
			'propinsi_nama'		=> 'Propinsi',
			'kab_nama'			=> 'Kabupaten',
			'kec_nama'			=> 'Kecamatan',
			'kel_nama'			=> 'Kelurahan',			
		); 
        $this->load->model("mdl_order","M");
 		$this->load->model("datatable_order","D");
		$this->css_file = array(
			'cropper/v0.7.9/css/cropper.min.css',
			'cropper/v0.7.9/css/main.css'
//			'cropper/cropper.min.css'
		);
		
		//load js..
		$this->js_plugins = array(
			'plugins/bootstrap/bootstrap-datepicker.js',
			'plugins/bootstrap/bootstrap-file-input.js',
			'plugins/bootstrap/bootstrap-select.js',
			'plugins/webcamjs/webcam.js',
			'plugins/fileinput/fileinput.min.js',
			'plugins/datatables/jquery.dataTables.min.js',
//			'plugins/blueimp/jquery.blueimp-gallery.min.js',
			'cropper/docs/v0.7.9/js/cropper.min.js'
//			'cropper/docs/v0.7.9/js/main2.js'
//			'plugins/dropzone/dropzone.min.js',
//			'plugins/filetree/jqueryFileTree.js',
//			'plugins/jstree/jstree.min.js',
//			'plugins/cropper/cropper.min.js'
		);
		
		$this->js_file = array(
			'cropper/docs/v0.7.9/js/main.js'
		);
		}

    
    function _reset(){		
		$this->jCfg['search'] = $this->sCfg['search'] = array(
								'class'		=> $this->_getClass(),
								'per_page'	=> 10,
								'result'	=> '',
								'propinsi'	=> '',
								'kabupaten'	=> '',
								'kecamatan'	=> '',
								'kelurahan'	=> '',
								'pengusul'	=> '',
								'nama'		=> '',
								'status'	=> '',
								'angkatan'	=> '',
								'unsur'		=> '',
								'nik'		=> '',
								'npapg'		=> '',
								'usulan'	=> ''
							);
							
        $this->jCfg['page_tab'] = $this->sCfg['page_tab'] = '1';
        $this->jCfg['type_data'] = $this->sCfg['type_data'] = 1;
        $this->_releaseSession();
    }

    function set_tab(){
        $tab = $this->input->get('tab');
        $this->sCfg['type_data'] = $tab;
        $this->_releaseSession();

        $next = $this->own_link;
        if(isset($_GET['next'])){
            $next = $_GET['next'];
        }

        redirect($next);
    }
    
    function index() {

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		
		$data = array('data' => array());
		if($this->input->post('btn_search')){			
			if($this->input->post('propinsi') && trim($this->input->post('propinsi'))!="")
				$this->jCfg['search']['propinsi'] = $this->sCfg['search']['propinsi'] = $this->input->post('propinsi');
			else
				$this->jCfg['search']['propinsi'] = $this->sCfg['search']['propinsi'] = "";

			if($this->input->post('kabupaten') && trim($this->input->post('kabupaten'))!="")
				$this->jCfg['search']['kabupaten'] = $this->sCfg['search']['kabupaten'] = $this->input->post('kabupaten');
			else
				$this->jCfg['search']['kabupaten'] = $this->sCfg['search']['kabupaten'] = "";

			if($this->input->post('angkatan') && trim($this->input->post('angkatan'))!="")
				$this->jCfg['search']['angkatan'] = $this->sCfg['search']['angkatan'] = $this->input->post('angkatan');
			else
				$this->jCfg['search']['angkatan'] = $this->sCfg['search']['angkatan'] = "";	

			if($this->input->post('usulan') && trim($this->input->post('usulan'))!="")
				$this->jCfg['search']['usulan'] = $this->sCfg['search']['usulan'] = $this->input->post('usulan');
			else
				$this->jCfg['search']['usulan'] = $this->sCfg['search']['usulan'] = "";	

			if($this->input->post('unsur') && trim($this->input->post('unsur'))!="")
				$this->jCfg['search']['unsur'] = $this->sCfg['search']['unsur'] = $this->input->post('unsur');
			else
				$this->jCfg['search']['unsur'] = $this->sCfg['search']['unsur'] = "";
				
			if($this->input->post('npapg') && trim($this->input->post('npapg'))!="")
				$this->jCfg['search']['npapg'] = $this->sCfg['search']['npapg'] = $this->input->post('npapg');
			else
				$this->jCfg['search']['npapg'] = $this->sCfg['search']['npapg'] = "";

			if($this->input->post('nik') && trim($this->input->post('nik'))!="")
				$this->jCfg['search']['nik'] = $this->sCfg['search']['nik'] = $this->input->post('nik');
			else
				$this->jCfg['search']['nik'] = $this->sCfg['search']['nik'] = "";

			$this->jCfg['search']['result'] = 1;
			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}
			$data = $this->_data(array(
					"base_url"	=> $this->own_link.'/index/'
				));			
		$this->_v($this->folder_view.$this->prefix_view,$data);
		
    }
	
	function add(){	
		$this->breadcrumb[] = array(
				"title"		=> "Add"
			);		
		$this->_v($this->folder_view.$this->prefix_view."_form",array());
	}
    function edit(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'order_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_form",array());
		}else{
			redirect($this->own_link);
		}

    }
	
    function confirm(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'order_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_confirm",array());
		}else{
			redirect($this->own_link);
		}

    }	
	
    function detail(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'order_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_detail",array());
		}else{
			redirect($this->own_link);
		}

    }	

    function payment(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Payment"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'order_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_payment",array());
		}else{
			redirect($this->own_link);
		}

    }

    function update_order(){ 

		$this->breadcrumb[] = array(
				"title"		=> "Update Order"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'order_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_update",array());
		}else{
			redirect($this->own_link);
		}

    }

	function save(){
		
		$amount =0;
		$data = array(
			'order_date'				=> $this->input->post('order_date'),
			'order_customer'			=> $this->input->post('order_customer'),
			'order_pick_up'				=> $this->input->post('order_pick_up'),
			'order_day'					=> $this->input->post('order_day'),
			'order_container_number'	=> $this->input->post('order_container_number'),
			'order_armada'				=> $this->input->post('order_armada'),
			'order_driver'				=> $this->input->post('order_driver'),
			'order_destination'			=> $this->input->post('order_destination'),
			'order_pic'					=> $this->jCfg['user']['fullname'],
			'order_user'				=> $this->jCfg['user']['name'],
			'order_id_user'				=> $this->jCfg['user']['id'],
			'order_status'				=> 0
		);				
		$a = $this->_save_master( 
			$data,
			array(
				'order_id' => dbClean($_POST['order_id'])
			),
			dbClean($_POST['order_id'])			
		);
		$id = $a['id'];
		if(empty($_POST['order_id'])){
			foreach((array)get_jo() as $kj=>$vj){
			$m = "insert";
			$this->db->insert("trx_order_detail", array(
								"order_id" 			=> $id,
								"order_code" 		=> "",
								"jo_id" 			=> $vj->jo_id,
								"amount" 			=> $this->input->post('biaya-'.$vj->jo_id)*$this->input->post('order_day'),
					 ));
			}
		}else{
			foreach((array)get_jo() as $kj=>$vj){
			$m = "update";
			$this->db->update("trx_order_detail", array(
								"amount" 			=> $this->input->post('biaya-'.$vj->jo_id)*$this->input->post('order_day'),
							 ),array(
								"order_id" 			=> $id,
								"jo_id" 			=> $vj->jo_id,							 
							 ));
			}
		}
		$this->db->insert("app_changelog", array(
							"changelog_date" 	=> date("Y-m-d H:i:s"),
							"changelog_menu" 	=> "master_order",
							"changelog_data" 	=> $id,
							"changelog_desc" 	=> "tambah/edit/update order",
							"changelog_text" 	=> "tambah/edit/update order",
							"changelog_request" => json_encode($data),
							"changelog_user" 	=> $this->jCfg['user']['fullname'],
							"changelog_user_id" => $this->jCfg['user']['id'],
						 ));
		redirect($this->own_link."/confirm?_id="._encrypt($id));
	}    
	function proceed(){
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));
		$this->db->update("trx_order", array(
								"order_amount" 			=> get_total_amount($id)->amount,
								"order_status" 			=> 1,
							 ),array(
								"order_id" 			=> $id,
							 ));
		redirect($this->own_link."/detail?_id="._encrypt($id));
	} 	
	function update_payment(){
		$this->db->update("trx_order", array(
							"order_status" 			=> 2,
						 ),array(
							"order_id" 			=> $this->input->post('order_id'),
						 ));
		$desc = "Penerimaan Pembayaran Stuffing ".$this->input->post('order_customer')." ".$this->input->post('order_code');
		$this->db->insert("trx_jurnal", array(
								"jurnal_date" 			=> $this->input->post('payment_date'),
								"jurnal_account"		=> $this->input->post('akun'),
								"jurnal_desc" 			=> $desc,
								"jurnal_debit" 			=> $this->input->post('order_amount'),
								"col1" 					=> $this->input->post('order_id'),
								"jurnal_status" 		=> 1,
								"jurnal_user" 			=> $this->jCfg['user']['fullname'],
								"jurnal_id_user" 		=> $this->jCfg['user']['id'],
							 ));
		redirect($this->own_link."/detail?_id="._encrypt($this->input->post('order_id')));
	} 	
	function update_pickup(){
		$this->db->update("trx_order", array(
							"order_status" 			=> 3,
						 ),array(
							"order_id" 			=> $this->input->post('order_id'),
						 ));
		redirect($this->own_link."/update_order?_id="._encrypt($this->input->post('order_id')));
	}
	function update_biaya(){
		$this->db->insert("trx_jurnal", array(
								"jurnal_date" 			=> $this->input->post('trx_date'),
								"jurnal_account"		=> $this->input->post('kas'),
								"jurnal_desc" 			=> "Penarikan Kas untuk Biaya Stuffing",
								"jurnal_kredit"			=> $this->input->post('biaya'),
								"col1" 					=> $this->input->post('order_id'),
								"jurnal_status" 		=> 1,
								"jurnal_user" 			=> $this->jCfg['user']['fullname'],
								"jurnal_id_user" 		=> $this->jCfg['user']['id'],
							 ));
		$this->db->insert("trx_jurnal", array(
								"jurnal_date" 			=> $this->input->post('trx_date'),
								"jurnal_account"		=> $this->input->post('akun'),
								"jurnal_desc" 			=> $this->input->post('trx_desc'),
								"jurnal_kredit"			=> $this->input->post('biaya'),
								"col1" 					=> $this->input->post('order_id'),
								"jurnal_status" 		=> 1,
								"jurnal_user" 			=> $this->jCfg['user']['fullname'],
								"jurnal_id_user" 		=> $this->jCfg['user']['id'],
							 ));
		redirect($this->own_link."/update_order?_id="._encrypt($this->input->post('order_id')));
	}
	function update_close(){
		$this->db->update("trx_order", array(
							"order_status" 			=> 4,
							"order_done_date"		=> date("Y-m-d"),
						 ),array(
							"order_id" 			=> $this->input->post('order_id'),
						 ));
		redirect($this->own_link."/update_order?_id="._encrypt($this->input->post('order_id')));
	}
    function delete(){
        $id=_decrypt(dbClean(trim($this->input->get('_id'))));
        if(trim($id) != ''){
            $o = $this->DATA->_delete(
                array("order_id"  => idClean($id)),
                TRUE
            );
            
        }
        redirect($this->own_link."?msg=".urldecode('Delete data KTA succes')."&type_msg=success");
    }
    
    function update(){
        $id=$this->input->post('id');
        if(trim($id) != ''){
            $o = $this->DATA->_update(
            	array("order_id"  => idClean($id)),
            	array("is_cetak"  		 => 1,
					  "time_print_card"  => date("Y-m-d H:i:s"),
					  "col14"  			 => $this->jCfg['user']['id'],
					  )
            );
            
            echo $o;
        }
    }
    function ajax_list()
    {
        $list = $this->D->get_datatables();
        $data = array();
        $no = $_POST['start'];
		$status = cfg('status-order');
        foreach ($list as $customers) {	
		switch($customers->order_status){
			case 0:
				$s = 'font-info';
				$action = "<a href='".$this->own_link."/confirm/?_id="._encrypt($customers->order_id)."' class='font-info'>confirm</a> | <a href='".$this->own_link."/delete/?_id="._encrypt($customers->order_id)."' class='font-danger'>cancel </a> | <a href='".$this->own_link."/detail/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
			case 1:
				$s = 'font-success';
				$action = "<a href='".$this->own_link."/payment/?_id="._encrypt($customers->order_id)."' class='font-info'>payment</a> | <a href='".$this->own_link."/delete/?_id="._encrypt($customers->order_id)."' class='font-danger'>cancel</a> | <a href='".$this->own_link."/detail/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
			case 2:
				$s = 'font-warning';
				$action = "<a href='".$this->own_link."/update_order/?_id="._encrypt($customers->order_id)."' class='font-info'>update</a> | <a href='".$this->own_link."/detail/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
			case 3:
				$s = 'font-warning';
				$action = "<a href='".$this->own_link."/update_order/?_id="._encrypt($customers->order_id)."' class='font-info'>update</a> | <a href='".$this->own_link."/detail/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
			case 4:
				$s = 'font-danger';
				$action = "<a href='".$this->own_link."/update_order/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
			default:
				$s = 'font-default';
				$action = "<a href='".$this->own_link."/detail/?_id="._encrypt($customers->order_id)."' class='font-primary'>detail</a>";
				break;
		}
            $no++;
            $row = array();
            $row[] 	= $no;
            $row[] 	= $customers->order_date;
            $row[] 	= $customers->order_code;
            $row[] 	= $customers->customer_name;
            $row[] 	= $customers->tujuan;
            $row[] 	= $customers->plat_nomor." / ". $customers->driver_name;
            $row[] 	= myNum($customers->order_amount);
            $row[] 	= '<span class="'.$s.'">'.$status[$customers->order_status].'</span';
            $row[] 	= $action;
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->D->count_all(),
                        "recordsFiltered" => $this->D->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }	
}