<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Cek_kesiapan_data extends AdminController {  
	function __construct()    
	{
		parent::__construct(); 
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM"); //"view"
		$this->_set_title('Cek Kesiapan Data SIPOL');
		$this->DATA->table="app_kta";
		$this->folder_view = "verifikasi/";
		$this->prefix_view = strtolower($this->_getClass());

		$this->breadcrumb[] = array(
				"title"		=> "Cek Kesiapan Data Sipol",
				"url"		=> $this->own_link
			);

		$this->is_search_date = false;

		if(!isset($this->jCfg['search']['class']) || $this->jCfg['search']['class'] != $this->_getClass()){
			$this->_reset();
			redirect($this->own_link);
		}
		
	
		$this->cat_search = array(
			''						=> 'All',
			'kab_nama'				=> 'Nama Kabupaten',
			'propinsi_nama'			=> 'Nama Provinsi'
			
		); 
		$this->load->model("mdl_verifikasi","M");

		//load js..
		$this->js_plugins = array(
			'plugins/bootstrap/bootstrap-datepicker.js',
			'plugins/bootstrap/bootstrap-file-input.js',
			'plugins/datatables/jquery.dataTables.min.js',
			'plugins/bootstrap/bootstrap-select.js'
		);
	}

	
	function _reset(){
		$this->sCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'kab_kode',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
			$data = $this->_data(array(
					"base_url"	=> $this->own_link.'/index'
			));
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function search(){

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);

		if($this->input->post('btn_reset')){
			$this->_reset();
		}
		$par_filter = array(
				"propinsi"	=> $this->input->post('propinsi'),
				"kabupaten"	=> $this->input->post('kabupaten'),
				"kecamatan"	=> $this->input->post('kecamatan'),
				"kelurahan"	=> $this->input->post('kelurahan'),
			);
				
			$this->data_table = $this->M->cek_kesiapan($par_filter);
			$data = $this->_data(array(
					"base_url"	=> $this->own_link.'/index'
			));
//			debugCode($data);
			$data['param'] = array(
				"propinsi"	=> $this->input->post('propinsi'),
				"kabupaten"	=> $this->input->post('kabupaten'),
				"kecamatan"	=> $this->input->post('kecamatan'),
				"kelurahan"	=> $this->input->post('kelurahan'),
			);
			$this->_v($this->folder_view.$this->prefix_view,$data);			
	}
}