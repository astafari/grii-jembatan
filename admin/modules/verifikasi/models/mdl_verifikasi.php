<?php
class mdl_verifikasi extends CI_Model{ 
		
	function __construct(){
		parent::__construct();
	} 

	function cek_ganda($p=array(),$count=FALSE){
		
		$total = 0;

		$this->db->select('app_kta.*,
							 propinsi_nama, kab_nama, kec_nama, kel_nama, nama_pengguna');		
		$this->db->join('app_propinsi','app_propinsi.propinsi_kode=app_kta.kta_propinsi','LEFT');
		$this->db->join('app_kabupaten','app_kabupaten.kab_kode=app_kta.kta_kabupaten','LEFT');
		$this->db->join('app_kecamatan','app_kecamatan.kec_kode=app_kta.kta_kecamatan','LEFT');
		$this->db->join('app_kelurahan','app_kelurahan.kel_kode=app_kta.kta_kelurahan','LEFT');
		$this->db->join('app_pengguna','app_pengguna.penggunaID=app_kta.kta_pemesan','LEFT');
		$this->db->where('kta_no_id IN (
								SELECT kta_no_id
								FROM app_kta
								GROUP BY kta_no_id
								HAVING count(kta_no_id) > 1
								)		
		');
		$this->db->order_by('kta_no_id','ASC');
		
		$qry = $this->db->get('app_kta');
		if($count==FALSE){
			$total = $this->cek_ganda($p,TRUE);
			return array(
					"data"	=> $qry->result(),
					"total" => $total
				);
		}else{
			return $qry->num_rows();
		}
		

	}
	function cek_kesiapan($p=array(),$count=FALSE){
		
		$total = 0;

		$this->db->select('app_kta.*, propinsi_nama, kab_nama, kec_nama, kel_nama, nama_pengguna');		
		$this->db->join('app_propinsi','app_propinsi.propinsi_kode=app_kta.kta_propinsi','LEFT');
		$this->db->join('app_kabupaten','app_kabupaten.kab_kode=app_kta.kta_kabupaten','LEFT');
		$this->db->join('app_kecamatan','app_kecamatan.kec_kode=app_kta.kta_kecamatan','LEFT');
		$this->db->join('app_kelurahan','app_kelurahan.kel_kode=app_kta.kta_kelurahan','LEFT');
		$this->db->join('app_pengguna','app_pengguna.penggunaID=app_kta.kta_pemesan','LEFT');
		
		if(isset($p['propinsi']) ){
			if($p['propinsi'] != ""){
			$this->db->where("kta_propinsi",$p['propinsi']);
			}
		}

		if(isset($p['kabupaten']) ){
			if($p['kabupaten'] != ""){
			$this->db->where("kta_kabupaten",$p['kabupaten']);
			}
		}
		
		if(isset($p['kecamatan']) ){
			if($p['kecamatan'] != ""){
			$this->db->where("kta_kecamatan",$p['kecamatan']);
			}
		}
		if(isset($p['kelurahan']) ){
			if($p['kelurahan'] != ""){
			$this->db->where("kta_kelurahan",$p['kelurahan']);
			}
		}

		$this->db->order_by('kta_no_id','ASC');		
		$qry = $this->db->get('app_kta');
		if($count==FALSE){
			$total = $this->cek_kesiapan($p,TRUE);
			return array(
					"data"	=> $qry->result(),
					"total" => $total
				);
		}else{
			return $qry->num_rows();
		}
		

	}	
}