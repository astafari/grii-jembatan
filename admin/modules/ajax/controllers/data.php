<?php
include_once(APPPATH."libraries/FrontController.php");
class Data extends FrontController {

	function __construct()  
	{
		parent::__construct(); 		
	}
	
	function index() {}
	
	function kabupaten(){
		
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');

		$this->db->order_by("kab_nama");
		$m = $this->db->get_where("app_kabupaten",array(
				"kab_propinsi_id"	=> $prov,
				"kab_status"		=> 0
			))->result();

		$html = "<option value=''> - pilih kabupaten - </option>";
		foreach ((array)$m as $k => $v) {
			$s = $v->kab_kode==$kab?'selected="selected"':'';
			$html .= "<option value='".$v->kab_kode."' $s >".$v->kab_nama."</option>";
		}

		die($html);
	}
	
	function update_status(){
		
		$id = $this->input->post('id');

		$m = $this->db->update("app_export_sipol",array(
				"export_status"		=> 1,
				),array(
				"id"			=> $id,											
				));						
//		debugCode($m);
		if($m == 1){
			$html = '<span class="label label-success label-form" data-toggle="tooltip" data-placement="top" title data-original-title="Sudah di Download"><li class="fa fa fa-check"></li></span>';
		}
		die($html);
	}

	function ktp(){
		
		$ktp = $this->input->post('ktp');
		$this->db->order_by("kta_no_id");
		$this->db->select("kta_no_id");
		$m = $this->db->get_where("app_kta", array( 
			"kta_no_id" => $ktp
		))->result();
		if(count($m) == 0){
			echo "1";
		}else{
			echo '<font color="red">No. <strong>'.$ktp.'</strong>'.' telah didaftarkan.</font>';			
		}
	}

	function npapg(){
		
		$npapg = $this->input->post('npapg');

		$get_max = $this->db->query("
			select max(SUBSTRING(kta_nomor,-5)) as nomor
			from app_kta where kta_nomor !='' AND kta_kelurahan = '".$npapg."'
		")->row();

		$get_kec = $this->db->query("
			select kel_kec_id 
			from app_kelurahan where kel_id = '".$npapg."'
		")->row();
		
		$idx 				= isset($get_max->nomor)&&trim($get_max->nomor)!=0?(int)$get_max->nomor+1:1;
		$nomor_kta 			= str_repeat("0", 6-strlen($idx)).$idx;
		$nk					= substr($nomor_kta,2,4);
		$nomor_kartu 		= $get_kec->kel_kec_id.$npapg.$nk;

		$this->db->order_by("kta_nomor_kartu");
		$m = $this->db->get_where("app_kta", array( 
			"kta_nomor_kartu" => $nomor_kartu
		))->result();
		if(count($m) == 0){
			echo $get_kec->kel_kec_id.$npapg.$nk;
		}else{
			echo '<font color="red">No. <strong>'.$nomor_kartu.'</strong>'.' telah didaftarkan.</font>';			
		}
	}

	function username(){
		
		$ktp = $this->input->post('ktp');
		$this->db->order_by("user_name");
		$m = $this->db->get_where("app_user", array( 
			"user_name" => $ktp
		))->result();
		if(count($m) == 0){
			echo "1";
		}else{
			echo '<font color="red">Username <strong>'.$ktp.'</strong>'.' telah didaftarkan.</font>';			
		}
	}

	function card_print(){
		$npapg 	= $this->input->post('npapg');
		$status = $this->input->post('status');
/*		$this->db->order_by("kta_nomor_kartu");
		$m = $this->db->get_where("app_kta", array( 
			"kta_nomor_kartu" => $npapg
		))->result();
*/		$data = array(
			'is_cetak' => 1,
			'time_print_card' => date('Y-m-d H:i:s')						
		);		
		$this->db->set($data);
		$sql = $this->db->update("app_kta",$data,"'kta_nomor_kartu' = '".$npapg."'");
//		debugCode($this->db->update());
		echo "1";
	}

	function user_koordinator(){
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');

		$this->db->order_by("user_name");
		$m = $this->db->get_where("app_user",array(
				"col1"	=> $prov,
			))->result();

		$html = "<option value=''> - pilih operator data - </option>";
		foreach ((array)$m as $k => $v) {
			$s = $v->user_id==$kab?'selected="selected"':'';
			$html .= "<option value='".$v->user_id."' $s >".$v->user_fullname."</option>";
		}

		die($html);
	}

	function suket(){
		
		$ktp = $this->input->post('ktp');
		$this->db->order_by("kta_no_suket");
		$m = $this->db->get_where("app_kta", array( 
			"kta_no_suket" => $ktp
		))->result();
		if(count($m) == 0){
			echo "1";
//			echo '&nbsp;<img src="'.base_url().'assets/images/tick.gif" align="absmiddle"> Data KTP dapat digunakan';
		}else{
			echo '<font color="red">No. <strong>'.$ktp.'</strong>'.' telah didaftarkan.</font>';			
		}
//		$html = "<option value=''> - pilih kabupaten - </option>";
//		die($html);
	}

	function kecamatan(){
		
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');

		$this->db->order_by("kec_nama");
		$m = $this->db->get_where("app_kecamatan",array(
				"kec_kab_id"	=> $prov
			))->result();

		$html = "<option value=''> - pilih kecamatan - </option>";
		foreach ((array)$m as $k => $v) {
			$s = $v->kec_kode==$kab?'selected="selected"':'';
			$html .= "<option value='".$v->kec_kode."' $s >".$v->kec_nama."</option>";
		}

		die($html);
	}
	
	function batch(){
		
		$batch = $this->input->post('batch');

		$this->db->order_by("kta_nomor_kartu");
		$m = $this->db->get_where("app_kta",array(
				"col15"	=> $batch
			))->result();

		$html .= "<div class='table-responsive'>";
		$html .= "<table class='table table-hover table-bordered table-striped'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th width='30px'>No</th>";
		$html .= "<th>Nomor Kartu</th>";
		$html .= "<th>Nama Lengkap</th>";
		$html .= "<th>Domisili</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>"; 
				foreach ((array)$m as $k => $v) {
			$get_kab = $this->db->query("
				select kab_nama 
				from app_kabupaten where kab_kode = '".$v->kta_kabupaten."'
			")->row();
			$get_prop = $this->db->query("
				select propinsi_nama 
				from app_propinsi where propinsi_kode = '".$v->kta_propinsi."'
			")->row();
		$html .= "<tr>";
		$html .= "<td>".++$no."</td>";
		$html .= "<td>".substr($v->kta_nomor_kartu,0,6)." ".substr($v->kta_nomor_kartu,6,6)." ".substr($v->kta_nomor_kartu,12,4)."</td>";
		$html .= "<td>".$v->kta_nama_lengkap."</td>";
		$html .= "<td>".$get_kab->kab_nama." - ".$get_prop->propinsi_nama."</td>";
		$html .= "</tr>";
				}						
		$html .= "</tbody>";
		$html .= "</table>";
		$html .= "</div>";
		die($html);
	}	

	function get_growing_data(){
		$filter 		= $this->input->post('filter');
		$awal 			= $this->input->post('awal');
		$akhir 			= $this->input->post('akhir');
		$search_date 	= "";
		if(empty($awal)){
			$search_date = "WHERE DATE_FORMAT(app_kta.time_add, '%Y-%m-%d') <= '". $akhir."'";
		}elseif(empty($akhir)){
			$search_date = "WHERE DATE_FORMAT(app_kta.time_add, '%Y-%m-%d') >= '". $awal."'";
		}elseif(empty($akhir) && empty($akhir) ){
			$search_date = "WHERE DATE_FORMAT(app_kta.time_add, '%Y-%m-%d') <= '".date("Y-m-d")."'";
		}else{
			$search_date = "WHERE DATE_FORMAT(app_kta.time_add, '%Y-%m-%d') BETWEEN '".$awal."' AND '".$akhir."'";			
		}
		$html = "";
		if(empty($filter) || $filter == "1"){
			$data = $this->db->query("
				select
					propinsi_nama,  
					COUNT(if(kta_status_data = '0', kta_id, NULL)) as entry,
					COUNT(if(kta_status_data = '1', kta_id, NULL)) as approve,
					COUNT(if(kta_status_data = '2', kta_id, NULL)) as upload,
					COUNT(if(kta_status_data = '3', kta_id, NULL)) as reject_upload,
					COUNT(if(kta_status_data = '4', kta_id, NULL)) as reject_entry,
					COUNT(if(is_cetak 		 = '0', kta_id, NULL)) as belum_cetak,
					COUNT(if(is_cetak 		 = '1', kta_id, NULL)) as cetak,
					COUNT(if(is_cetak 		 = '2', kta_id, NULL)) as siap_cetak,
					COUNT(if(is_cetak 		 = '3', kta_id, NULL)) as kirim_pabrik,
					COUNT(if(is_cetak 		 = '4', kta_id, NULL)) as terima_pabrik,
					COUNT(if(is_cetak 		 = '5', kta_id, NULL)) as kirim,
					COUNT(if(is_bayar 		 = '0', kta_id, NULL)) as belum_bayar,
					COUNT(if(is_bayar 		 = '1', kta_id, NULL)) as bayar,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode
				$search_date 
				ORDER BY kta_propinsi ASC
			")->result();					
			$cetak = $this->db->query("
				select
					propinsi_nama,  
					COUNT(if(is_cetak 		 = '0', kta_id, NULL)) as belum_cetak,
					COUNT(if(is_cetak 		 = '1', kta_id, NULL)) as cetak,
					COUNT(if(is_cetak 		 = '2', kta_id, NULL)) as siap_cetak,
					COUNT(if(is_cetak 		 = '3', kta_id, NULL)) as kirim_pabrik,
					COUNT(if(is_cetak 		 = '4', kta_id, NULL)) as terima_pabrik,
					COUNT(if(is_cetak 		 = '5', kta_id, NULL)) as kirim,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode
				$search_date 
				AND kta_status_data != 3 AND kta_status_data != 4			
				ORDER BY kta_propinsi ASC
			")->result();					
			$bayar = $this->db->query("
				select
					propinsi_nama,  
					COUNT(if(is_bayar 		 = '0', kta_id, NULL)) as belum_bayar,
					COUNT(if(is_bayar 		 = '1', kta_id, NULL)) as bayar,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode
				$search_date 
				AND kta_status_data != 3 AND kta_status_data != 4			
				ORDER BY kta_propinsi ASC
			")->result();		}else{
			$data = $this->db->query("
				select
					nama_pengguna,  
					COUNT(if(kta_status_data = '0', kta_id, NULL)) as entry,
					COUNT(if(kta_status_data = '1', kta_id, NULL)) as approve,
					COUNT(if(kta_status_data = '2', kta_id, NULL)) as upload,
					COUNT(if(kta_status_data = '3', kta_id, NULL)) as reject_upload,
					COUNT(if(kta_status_data = '4', kta_id, NULL)) as reject_entry,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				LEFT JOIN app_pengguna ON app_kta.kta_pemesan = app_pengguna.penggunaID
				$search_date 
				ORDER BY kta_pemesan ASC
			")->result();					
			$cetak = $this->db->query("
				select
					nama_pengguna,  
					COUNT(if(is_cetak 		 = '0', kta_id, NULL)) as belum_cetak,
					COUNT(if(is_cetak 		 = '1', kta_id, NULL)) as cetak,
					COUNT(if(is_cetak 		 = '2', kta_id, NULL)) as siap_cetak,
					COUNT(if(is_cetak 		 = '3', kta_id, NULL)) as kirim_pabrik,
					COUNT(if(is_cetak 		 = '4', kta_id, NULL)) as terima_pabrik,
					COUNT(if(is_cetak 		 = '5', kta_id, NULL)) as kirim,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				LEFT JOIN app_pengguna ON app_kta.kta_pemesan = app_pengguna.penggunaID
				$search_date 
				AND kta_status_data != 3 AND kta_status_data != 4			
				ORDER BY kta_pemesan ASC
			")->result();					
			$bayar = $this->db->query("
				select
					nama_pengguna,  
					COUNT(if(is_bayar 		 = '0', kta_id, NULL)) as belum_bayar,
					COUNT(if(is_bayar 		 = '1', kta_id, NULL)) as bayar,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				LEFT JOIN app_pengguna ON app_kta.kta_pemesan = app_pengguna.penggunaID
				$search_date 
				AND kta_status_data != 3 AND kta_status_data != 4			
				ORDER BY kta_pemesan ASC
			")->result();					
		}
			if( count($data) > 0 ) {
            	$no = 0; $cname = '';
                foreach($data as $r) {
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Upload</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->upload).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Entry</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->entry).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Approve</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->approve).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr style="background-color:#ccc;">';
					$html .= '	<td><b>TOTAL DATA (Upload + Entry + Approve)</b></td>';
					$html .= '	<td>'.myNum($r->upload+$r->entry+$r->approve).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Reject Upload</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->reject_upload).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Reject Entry</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->reject_entry).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr style="background-color:#ccc;">';
					$html .= '	<td><b>TOTAL DATA REJECT (Reject Upload + Reject Entry)</b></td>';
					$html .= '	<td>'.myNum($r->reject_upload+$r->reject_entry).'</td>';
                	$html .= '</tr>';
                }					
                  foreach($cetak as $c) {
					$html .= '<tr>';
					$html .= '	<td>Jumlah Kartu Belum Cetak</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($c->belum_cetak).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Kartu Sudah Cetak</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($c->cetak).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Kartu Proses Cetak</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($c->kirim_pabrik+$c->siap_cetak+$c->terima_pabrik+$c->kirim).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr style="background-color:#ccc;">';
					$html .= '	<td><b>TOTAL DATA (Belum Cetak + Sudah Cetak + Proses Cetak)</b></td>';
					$html .= '	<td>'.myNum($c->total).'</td>';
                	$html .= '</tr>';
                }					
                 foreach($bayar as $b) {
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Kartu Terbayar</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($b->bayar).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr>';
					$html .= '	<td>Jumlah Kartu Belum Terbayar</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($b->belum_bayar).'</td>';
                	$html .= '</tr>';
                	$html .= '<tr style="background-color:#ccc;">';
					$html .= '	<td><b>TOTAL DATA (Bayar + Belum Bayar)</b></td>';
					$html .= '	<td>'.myNum($b->total).'</td>';
                	$html .= '</tr>';
                }					
			}						
		die($html);
	}

	function get_area_data(){
		$filter 		= $this->input->post('filter');
		$area 			= $this->input->post('area');
		$search		 	= "";
		$html = "";
		if(empty($filter) || $filter == "1"){
			if(!empty($area)){
				$search		= "WHERE kta_propinsi = '".$area."' AND kta_status_data != 3 AND kta_status_data != 4";
			}else{
				$search		= "WHERE kta_status_data != 3 AND kta_status_data != 4";				
			}
			$data = $this->db->query("
				select
					propinsi_nama as nama,
					COUNT(if(kta_id, kta_id, NULL)) as jumlah, kta_propinsi as prop_kode
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode 
				$search
				GROUP BY
					kta_propinsi
				ORDER BY jumlah DESC
			")->result();					
		}else{
			if(!empty($area)){
				$search		= "WHERE kta_pemesan = '".$area."' AND kta_status_data != 3 AND kta_status_data != 4";
			}else{
				$search		= "WHERE kta_status_data != 3 AND kta_status_data != 4";				
			}
			$data = $this->db->query("
				select
					nama_pengguna as nama, app_pengguna.propinsi_id as prop_kode, app_pengguna.tingkat_pengguna,
					COUNT(if(kta_id, kta_id, NULL)) as jumlah
				FROM
					app_kta
				LEFT JOIN app_pengguna ON app_kta.kta_pemesan = app_pengguna.penggunaID 
				$search
				GROUP BY
					kta_pemesan
				ORDER BY jumlah DESC
			")->result();					
		}
			if( count($data) > 0 ) {
            	$no = 0; $cname = ''; $total=0; $totalTarget=0;
                foreach($data as $r) {
					
					if($filter == "2" ){
						if($r->tingkat_pengguna == "DPD"){
							$kab = $this->db->query("
								select
									count(kab_kode) as jumlahkab
								FROM
									app_kabupaten
								WHERE kab_propinsi_id = '".$r->prop_kode."'
							")->row();					
							$targetPercent  = (($r->jumlah) / ($kab->jumlahkab * 1500)) * 100;					
							$target 		= $kab->jumlahkab * 1500;		
							$jumlah			= $r->jumlah." dari ".($kab->jumlahkab * 1500)." KTA";
						}else{
							$targetPercent  = 0;					
							$target 		= 0;															
							$jumlah			= $r->jumlah." KTA";
						}
					}else{
						$kab = $this->db->query("
							select
								count(kab_kode) as jumlahkab
							FROM
								app_kabupaten
							WHERE kab_propinsi_id = '".$r->prop_kode."'
						")->row();					
						$targetPercent  = (($r->jumlah) / ($kab->jumlahkab * 1500)) * 100;					
						$target 		= $kab->jumlahkab * 1500;						
						$jumlah			= $r->jumlah." dari ".($kab->jumlahkab * 1500)." KTA";
					}
						$total 		  = $total + $r->jumlah;			
						$totalTarget  = $totalTarget + $target;						
						$totalPercent = ($total / $totalTarget) * 100; 
					$no++;
                	$html .= '<tr>';
					$html .= '	<td>'.$no.'</td>';
					$html .= '	<td>'.$r->nama.'</td>';
					$html .= '	<td nowrap="nowrap">'.$jumlah.'</td>';
					$html .= '	<td nowrap="nowrap">'.round($targetPercent,2).' %</td>';
                	$html .= '</tr>';
                }
                	$html .= '<tr>';
					$html .= '	<td colspan="2" align="center"><b>TOTAL</b></td>';
					$html .= '	<td nowrap="nowrap">'.myNum($total).' dari '.myNum($totalTarget).' KTA</td>';
					$html .= '	<td nowrap="nowrap">'.round($totalPercent,2).' %</td>';
                	$html .= '</tr>';
//					debugCode($kab);					
			}						
		die($html);
	}
	function get_gender_data(){
		$area 			= $this->input->post('area');
		$html 			= "";

			if(!empty($area)){
				$search		= "WHERE kta_propinsi = '".$area."' AND app_kta.kta_status_data != 3 AND app_kta.kta_status_data != 4";
			}else{
				$search		= "WHERE app_kta.kta_status_data != 3 AND app_kta.kta_status_data != 4";				
			}

			$data = $this->db->query("
					select
						propinsi_nama as nama, 
						COUNT(if(kta_jenkel='1', kta_id, NULL)) as laki,
						COUNT(if(kta_jenkel='2', kta_id, NULL)) as perempuan			
					FROM
						app_propinsi
					INNER JOIN app_kta ON app_kta.kta_propinsi = app_propinsi.propinsi_kode
					$search					
					GROUP BY
						nama
					ORDER BY nama ASC
			")->result();					
			if( count($data) > 0 ) {
            	$no = 0; $cname = ''; $total=0; $totalTarget=0;
                foreach($data as $r) {
					$totalArea		= $r->laki + $r->perempuan;
					$total 		  	= $total + $totalArea;			
					$lakiPercent	= ($r->laki / $totalArea) * 100;					
					$perPercent		= ($r->perempuan / $totalArea) * 100;					
					$no++;
                	$html .= '<tr>';
					$html .= '	<td>'.$no.'</td>';
					$html .= '	<td>'.$r->nama.'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->laki).' ('.round($lakiPercent).'%)</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->perempuan).' ('.round($perPercent).'%)</td>';
                	$html .= '</tr>';
                }
			}	
			
		die($html);
	}	
	function get_status_data(){
		$area 			= $this->input->post('area');
		$html 			= "";

			if(!empty($area)){
				$search		= "WHERE kta_pemesan = '".$area."'";
			}

			$data = $this->db->query("
				select
					nama_pengguna as nama,  
					COUNT(if(kta_status_data = '0', kta_id, NULL)) as entry,
					COUNT(if(kta_status_data = '1', kta_id, NULL)) as approve,
					COUNT(if(kta_status_data = '2', kta_id, NULL)) as upload,
					COUNT(if(kta_status_data = '3', kta_id, NULL)) as reject_upload,
					COUNT(if(kta_status_data = '4', kta_id, NULL)) as reject_entry,
					COUNT(if(kta_id				  , kta_id, NULL)) as total			
				FROM
					app_kta
				LEFT JOIN app_pengguna ON app_kta.kta_pemesan = app_pengguna.penggunaID
				$search
				GROUP BY kta_pemesan
				ORDER BY kta_pemesan ASC
			")->result();					
			if( count($data) > 0 ) {
            	$no = 0; $cname = ''; $total=0; $totalTarget=0;
                foreach($data as $r) {
					$no++;
                	$html .= '<tr>';
					$html .= '	<td>'.$no.'</td>';
					$html .= '	<td>'.$r->nama.'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->upload).'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->reject_upload).'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->entry).'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->reject_entry).'</td>';
					$html .= '	<td nowrap="nowrap">'.myNum($r->approve).'</td>';
                	$html .= '</tr>';
                }
			}	
			
		die($html);
	}	

	function get_data(){
		$verifikasi 			= $this->input->post('verifikasi');
		$html = "";
			$data = $this->db->query("
				select
					kta_id, kta_no_id, kta_nama_lengkap, kta_nomor_kartu, app_kabupaten.kab_nama, app_propinsi.propinsi_nama
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode 
				INNER JOIN app_kabupaten ON app_kta.kta_kabupaten = app_kabupaten.kab_kode 
				WHERE
					kta_no_id LIKE '%".$verifikasi."%'
				OR
					kta_nomor_kartu LIKE '%".$verifikasi."%'
				OR
					kta_nama_lengkap LIKE '%".$verifikasi."%'
			")->result();					
			if( count($data) > 0 ) {
            	$no = 0; $cname = '';
                foreach($data as $r) {
					$no++;
                	$html .= '<tr>';
					$html .= '	<td>'.$r->kta_nomor_kartu.'</td>';
					$html .= '	<td>'.$r->kta_no_id.'</td>';
					$html .= '	<td>'.$r->kta_nama_lengkap.'</td>';
					$html .= '	<td>'.$r->kab_nama.' - '.$r->propinsi_nama.'</td>';
					$html .= '	<td><a href="'.site_url('master/event_pendaftaran').'/edit/?_id='._encrypt($r->kta_id).'"><span class="label label-default label-form" data-toggle="tooltip" data-placement="top" title data-original-title="See Detail"><li class="fa fa-search"></li></span></a></td>';
                	$html .= '</tr>';
                }
			}						
		die($html);
	}
	
	function get_verification_data(){
		$verifikasi 			= $this->input->post('verifikasi');
		$html = "";
			$data = $this->db->query("
				select
					kta_id, kta_no_id, kta_nama_lengkap, kta_nomor_kartu, app_kabupaten.kab_nama, app_propinsi.propinsi_nama
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode 
				INNER JOIN app_kabupaten ON app_kta.kta_kabupaten = app_kabupaten.kab_kode 
				WHERE
					kta_no_id LIKE '%".$verifikasi."%'
				OR
					kta_nomor_kartu LIKE '%".$verifikasi."%'
				OR
					kta_nama_lengkap LIKE '%".$verifikasi."%'
			")->result();					
			if( count($data) > 0 ) {
            	$no = 0; $cname = '';
                foreach($data as $r) {
					$no++;
                	$html .= '<tr>';
					$html .= '	<td>'.$r->kta_nomor_kartu.'</td>';
					$html .= '	<td>'.$r->kta_no_id.'</td>';
					$html .= '	<td>'.$r->kta_nama_lengkap.'</td>';
					$html .= '	<td>'.$r->kab_nama.' - '.$r->propinsi_nama.'</td>';
					$html .= '	<td><a href="'.site_url('meme/me').'/verifikasi/?_id='._encrypt($r->kta_id).'"><span class="label label-default label-form" data-toggle="tooltip" data-placement="top" title data-original-title="See Detail"><li class="fa fa-search"></li></span></a></td>';
                	$html .= '</tr>';
                }
			}						
		die($html);
	}
	
	function kelurahan(){
		
		$prov = $this->input->post('prov');
		$kab = $this->input->post('kab');

		$this->db->order_by("kel_nama");
		$m = $this->db->get_where("app_kelurahan",array(
				"kel_kec_id"	=> $prov
			))->result();

		$html = "<option value=''> - pilih kelurahan/desa - </option>";
		foreach ((array)$m as $k => $v) {
			$s = $v->kel_kode==$kab?'selected="selected"':'';
			$html .= "<option value='".$v->kel_kode."' $s >".$v->kel_nama."</option>";
		}

		die($html);
	}

	function pengusul(){
		
		$pengusul 	= $this->input->post('pengusul');
		$tingkat 	= $this->input->post('tingkat');

		$this->db->order_by("nama_pengguna");
		$m = $this->db->get_where("app_pengguna",array(
				"tingkat_pengguna"	=> $tingkat
			))->result();
		
		if($tingkat == "DPP"){
		$html = "<option value=''> - pilih CQ - </option>";			
		}else{
		$html = "<option value=''> - pilih Pengusul - </option>";						
		}

		foreach ((array)$m as $k => $v) {
			$s = $v->penggunaID==$pengusul?'selected="selected"':'';
			$html .= "<option value='".$v->penggunaID."' $s >".$v->nama_pengguna."</option>";
		}

		die($html);
	}
	
	function check_login() {
		$status = array(
					"status"	=> 0,
					"data"		=> array(),
					"message"	=> ''
				);			
			
		$u = $this->db->get_where("app_user",array("user_name" => $this->jCfg['user']['name']))->row();
		
		if($u->col2==0) {			
			$status = array(
						"status"	=> 1,
						"data"		=> array( "go_to" => site_url('auth/out') ),
						"message"	=> 'Please login again'
					);
		}
			
		die(json_encode($status));
	}
	function save_image() {
		$upload_dir = 'assets/collections/card/';  //implement this function yourself
		$img 	= $this->input->post('imgBase64');
		$name 	= $this->input->post('npapg');
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = $upload_dir.$name.".png";
		$success = file_put_contents($file, $data);
		$this->db->update("app_kta",array(
			"kta_kartu"				=> $name.".png",
		),array(
			"kta_nomor_kartu"		=> $name,											
		));
		echo "success";
	}
	
	function generate(){
		$propinsi 			= $this->input->post('propinsi');
		$kabupaten 			= $this->input->post('kabupaten');
		$html = "";
			$data = $this->db->query("
				select
					app_kta.*, app_kabupaten.kab_nama, app_propinsi.propinsi_nama
				FROM
					app_kta
				INNER JOIN app_propinsi ON app_kta.kta_propinsi = app_propinsi.propinsi_kode 
				INNER JOIN app_kabupaten ON app_kta.kta_kabupaten = app_kabupaten.kab_kode 
				WHERE kta_kabupaten = '".$kabupaten."' and kta_kartu = ''
				and col18 = '1'
				LIMIT 1
			")->result();					
			if( count($data) > 0 ) {
            	$no = 0; $cname = '';
                foreach($data as $val) {
					$qrcode  =  gen_qr_code(substr($val->kta_nomor_kartu,0,6).substr($val->kta_nomor_kartu,6,6).substr($val->kta_nomor_kartu,12,4));
					$html 	.=  '<div class="row" align="center">';
					$html 	.=	'<div class="col-md-11">';
					$html 	.=	'<div id="canvas-card" class="canvas">';
					$html 	.=	'<div class="content-lg">';
					$html 	.=	'<div style="clear:both;float:right;margin:8px 9px;">';
					$html 	.=	'<img src="'.get_image(base_url()."assets/images/block.jpg").'" style="height:38px; width:38px;" />';
					$html 	.=	'</div>';	
					$html 	.=	'<div style="clear:both;float:right;margin:3px 22px 0 0;">';
					$html 	.=	'<img alt="" src="'.get_image(base_url()."assets/collections/kta/photo/".$val->kta_foto_wajah).'" style="height:97px; width:74px;" >';
					$html 	.=	'</div>';
					$html 	.=	'<div style="clear:both;float:right;margin-right:22px;">';
					$html 	.=	'<div class="nama-lg">'.strtoupper($val->kta_nama_lengkap).'</div>';
					$html 	.=	'<div class="nomor-lg">NPAPG '.substr($val->kta_nomor_kartu,0,6)." ".substr($val->kta_nomor_kartu,6,4)." ".substr($val->kta_nomor_kartu,10,6).'</div>';
					$html 	.=	'<div class="domisili-lg">'.$val->kab_nama." - ".$val->propinsi_nama.'</div>';
					$html 	.=	'<div class="masa-lg">'.date('m/Y',strtotime($val->time_add)).'</div>';
					$html 	.=	'</div>';
					$html 	.=	'</div>';
					$html 	.=	'</div>';
					$html 	.=	'</div>';
					$html 	.=	'</div>';
                }
			}						
		die($html);
	}
	
	function upload(){
		$this->db->update("app_kta",array(
					"col18"				=> 0,
					),array(
					"kta_kabupaten"		=> $this->input->post('kabupaten'),											
					));		
		$this->load->library("PHPExcel");
        $fileName = time().$_FILES['file']['name'];         
        $config['upload_path'] = APPPATH.'assets/uploadAnggota/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls';
        $config['max_size'] = 100000;         
        $this->load->library('upload');
        $this->upload->initialize($config);         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
        $media = $this->upload->data('file');
        $inputFileName = APPPATH.'assets/uploadAnggota/'.$media['file_name'];
			try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
//				redirect($this->own_link."?msg=".urldecode('GAGAL UPLOAD. PASTIKAN FILE YANG DI UPLOAD .xls')."&type_msg=danger");
//                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            for ($row = 2; $row <= $highestRow; $row++){              
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				debugCode($rowData);
				$this->db->update("app_kta",array(
						"col18"				=> 1,
						"col20"				=> 0,
					),array(
						'kta_no_id'       	 	=> $rowData[0][4],
					));		
            }		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */