<?php
/* connection to master database */
$cfg['db']['hostname'] = '127.0.0.1';
$cfg['db']['username'] = 'root';
$cfg['db']['password'] = '';
$cfg['db']['database'] = 'db_grii';
$cfg['db']['port'] 	   = '';
$cfg['db']['dbdriver'] = "mysqli";

/* module location HMVC */
$config['folder_modules']	 = 'modules';
$config['modules_locations'] = array(
    getcwd().'/'.$config['folder_modules'].'/' => '../../'.$config['folder_modules'].'/',
);

$config['template_web'] 	= 'red-box';
$config['template_admin'] 	= 'new';
$config['template_admin'] 	= 'atlant';
$config['key_front_pass'] 	= 'p1351IndoNesiaYysywk';

/* myconfig */
$config['app_name'] 	= "GRII - ADMIN LANDING PAGE";
$config['app_version'] 	= "1.0.0";


$config['color_theme'] = '#019927';


$config['activeLog'] 		= true;
$config['activeChat'] 		= true;

$config['status'] = array(
		0	=> 'Non-Aktif',
		1	=> 'Aktif',
);
$config['status-armada'] = array(
		0	=> 'tidak aktif',
		1	=> 'aktif',
		2	=> 'rusak sebagian',		
		3	=> 'rusak berat',				
		4	=> 'rusak total',				
);

$config['status-order'] = array(
		0	=> 'draft',
		1	=> 'unpaid',
		2	=> 'paid',		
		3	=> 'proses',				
		4	=> 'done',				
);

$config['jenis_id'] = array(
		"KTP"	=> "KTP",
		"SIM"	=> "SIM",
		"AKTA" 	=> "AKTA"
	);

$config['tingkatan']	= array(
		1 => 'DPP',
		2 => 'DPD PROV.',
		3 => 'DPD KAB/KOTA',
		4 => 'PK KEC',
		5 => 'PL DS/KEL',
		6 => 'Bukan Pengurus'		
	);

$config['hasta_karya']	= array(
		'AMPI'	 		=> 'AMPI',
		'HWK'	  		=> 'HWK',
		'MDI'	  		=> 'MDI',
		'AL-HIDAYAH'	=> 'AL-HIDAYAH',
		'SATKAR ULAMA' 	=> 'SATKAR ULAMA'
	);
$config['trikarya']	= array(
		'KOSGORO'		  => 'KOSGORO',
		'MKGR' 	 		  => 'MKGR',
		'SOKSI'	  		  => 'SOKSI',
		'Bukan Trikarya'  => 'Bukan Trikarya'
	);
$config['sayap']	= array(
		'AMPG' 			=> 'AMPG',
		'KPPG'			=> 'KPPG',		
		'Bukan Orsa'	=> 'Bukan Orsa'		
	);
$config['jabatan']	= array(
		'0' => '',
		1 => 'Ketua',
		2 => 'Wakil Ketua',		
		3 => 'Sekretaris',		
		4 => 'Wakil Sekretaris',
		5 => 'Bendahara',
		6 => 'Wakil Bendahara',
		7 => 'Anggota Pleno',
		8 => 'Anggota Biasa'
	);

$config['status_nikah']	= array(
		1 => 'Belum Kawin',
		2 => 'Kawin',
		3 => 'Pernah Menikah'		
	);

$config['pendidikan'] = array(
        'SD' => "SD",
        'SMP' => "SMP / Sederajat",
        'SMA' => "SMA / Sederajat",
        'D3' => "D3",
        'S1' => "S1",
        'S2' => "S2",		
        'S3' => "S3",		
        'Lainnya' => "Lainnya"
);
$config['pekerjaan'] = array(
        1 => "Pelajar",
        2 => "Mahasiswa",
        3 => "Profesional",
        4 => "Pegawai Swasta",
        5 => "Wirausaha",
        6 => "Buruh",
		7 => "Pensiunan",
		8 => "Ibu Rumah Tangga",
		9 => "Petani",
		10 => "Nelayan",
		11 => "Lainnya",
);

$config['jenkel']	= array(
		1 => 'Laki-laki',
		2 => 'Perempuan',			
);
$config['status_data']	= array(
		0 => 'Entry',
		1 => 'Approve',			
		2 => 'Upload',
		3 => 'Reject Upload',			
		4 => 'Reject Entry',			
);

$config['domisili']	= array(
		'YA' 	=> 'YA',
		'TIDAK' => 'TIDAK'
);
	
$config['bulan'] = array(
	'01'	=> "01",
	'02'	=> "02",
	'03'	=> "03",
	'04'	=> "04",
	'05'	=> "05",
	'06'	=> "06",
	'07'	=> "07",
	'08'	=> "08",
	'09'	=> "09",
	'10'	=> "10",
	'11'	=> "11",
	'12'	=> "12"
);
$config['browser']	= array(
		'IE'		=> '9',
		'Firefox'	=> '50',
		'Opera'		=> '30',
		'Chrome'	=> '50',
		'Safari'	=> '',
		'Netscape'	=> ''
	);