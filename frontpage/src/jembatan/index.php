<?php include "top/header.php"; ?>
    <!-- ==== MAIN CONTENT ===== -->
    <div class="hero-slider-with-banners space-bottom-2 mt-4d875">
        <div class="container">
            <div class="row">
                <div class="col-wd-12 mb-4 mb-xl-0">
                    <div class="bg-gray-200 px-5 px-md-8 px-xl-0 pl-xl-10 pt-6 min-height-530">
                        <div class="js-slick-carousel u-slick"
                            data-pagi-classes="text-center u-slick__pagination u-slick__pagination mt-7">
                            <div class="js-slider">
                                <div class="hero-slider">
                                    <div class="d-block d-xl-flex media">
                                        <div class="hero__body media-body align-self-center mb-4 mb-xl-0">
                                            <div class="hero__pretitle text-uppercase text-gray-400 font-weight-bold mb-3"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="200">
                                                GRII
                                            </div>

                                            <h2 class="hero__title font-size-10 mb-3"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="300">
                                                <span class="hero__title--1 font-weight-bold d-block">Jembatan</span>
                                                <span class="hero__title--2 d-block font-weight-normal">Jejaring Menyebar Bantuan dan Informasi</span>
                                            </h2>

                                            <p class="hero__subtitle font-size-2 mb-5"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="400">Register until 28th May 2020
                                            </p>

                                            <a href="#" class="hero__btn btn btn-primary-green text-white btn-wide"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="500">Read More
                                            </a>
                                        </div>

                                        <div
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="600">
                                            <img src="../../assets/img/images/PENDAFTARAN KATALOG(FINAL)_001.png" height="250px" width="300px" class="img-fluid" alt="image-description">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="js-slider">
                                <div class="hero-slider">
                                    <div class="d-block d-xl-flex media">
                                        <div class="hero__body media-body align-self-center mb-4 mb-xl-0">
                                            <div class="hero__pretitle text-uppercase text-gray-400 font-weight-bold mb-3"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="200">
                                                Contact Person
                                            </div>

                                            <h2 class="hero__title font-size-10 mb-3"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="300">
                                                <span class="hero__title--1 font-weight-bold d-block">@DEBORAHTRICIA</span>
                                                <span class="hero__title--2 d-block font-weight-normal">0811 - 246 - 333</span>
                                            </h2>

                                            <p class="hero__subtitle font-size-2 mb-5"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="400">
                                            </p>

                                            <a href="#" class="hero__btn btn btn-primary-green text-white btn-wide"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="500">Read More
                                            </a>
                                        </div>

                                        <div
                                            data-scs-animation-in="fadeInUp"
                                            data-scs-animation-delay="600">
                                            <img src="../../assets/img/images/PENDAFTARAN KATALOG(FINAL)_003.png" height="250px" width="300px" class="img-fluid" alt="image-description">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="space-bottom-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5 col-lg-4 col-xl-3 mb-4 mb-md-0">
                    <div class="bg-img-hero min-height-440 rounded" style="background-image: url(https://placehold.it/300x440);">
                        <div class="p-5">
                            <h4 class="font-size-22">Teman Sore</h4>
                            <p></p>
                            <a href="single-product.php" class="text-dark font-weight-medium text-uppercase stretched-link">View All</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-8 col-xl-9">
                    <div class="mx-lg-8 mx-wd-4">
                        <div class="js-slick-carousel products no-gutters"
                            data-pagi-classes="d-lg-none text-center u-slick__pagination u-slick__pagination mt-5 mb-0"
                            data-arrows-classes="d-none d-lg-block u-slick__arrow u-slick__arrow-centered--y rounded-circle"
                            data-arrow-left-classes="fas fa-chevron-left u-slick__arrow-inner u-slick__arrow-inner--left ml-lg-n8 ml-wd-n4"
                            data-arrow-right-classes="fas fa-chevron-right u-slick__arrow-inner u-slick__arrow-inner--right mr-lg-n8 mr-wd-n4"
                            data-slides-show="4"
                            data-responsive='[{
                               "breakpoint": 1500,
                               "settings": {
                                 "slidesToShow": 3
                               }
                            }, {
                               "breakpoint": 1199,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }, {
                               "breakpoint": 554,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }]'>
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Acak Adut Kitchen</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@acakadut.kitchen</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Babi Kecap + Kentang / Telur Rebus</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_006.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">A Fun Pao</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@afun_pao</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Homemade Afun Pao, Fresh dibuat setiap hari</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_007.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Bakmi Rakyat</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@themierakyat</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Original dan Yamin</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>       
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_008.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Buns & Cakes</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@bunsncakes.id</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Homemade Breads, Buns, Cookies & Cupcakes</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_009.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Dapur Lisa</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@dapurlisa</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Homecooked Comfort Food</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>                                               
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_010.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">D'Chicken</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@dchickenkuliner</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Chicken Biasa & Geprek</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="#" class="d-block"><img src="../../assets/img/images/TEMAN SORE_007.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Teman Sore</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Durian Palu Montong</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="#" class="text-gray-700">@durianAAofficial</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Durian Palu montong</span>                                                
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="#" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="View Product">
                                                <span class="product__add-to-cart">View Product</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                                                                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="space-bottom-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5 col-lg-4 col-xl-3 mb-4 mb-md-0">
                    <div class="bg-img-hero min-height-440 rounded" style="background-image: url(https://placehold.it/300x440);">
                        <div class="p-5">
                            <h4 class="font-size-22">GKY Sunter</h4>
                            <p>Berbagi Info Usaha</p>
                            <a href="#" class="text-dark font-weight-medium text-uppercase stretched-link">View All</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-8 col-xl-9">
                    <div class="mx-lg-8 mx-wd-4">
                        <div class="js-slick-carousel products no-gutters"
                            data-pagi-classes="d-lg-none text-center u-slick__pagination u-slick__pagination mt-5 mb-0"
                            data-arrows-classes="d-none d-lg-block u-slick__arrow u-slick__arrow-centered--y rounded-circle"
                            data-arrow-left-classes="fas fa-chevron-left u-slick__arrow-inner u-slick__arrow-inner--left ml-lg-n8 ml-wd-n4"
                            data-arrow-right-classes="fas fa-chevron-right u-slick__arrow-inner u-slick__arrow-inner--right mr-lg-n8 mr-wd-n4"
                            data-slides-show="4"
                            data-responsive='[{
                               "breakpoint": 1500,
                               "settings": {
                                 "slidesToShow": 3
                               }
                            }, {
                               "breakpoint": 1199,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }, {
                               "breakpoint": 554,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }]'>
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_003.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Pie Kacang Hijau Spesial</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 5.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_004.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Pepes Bandeng Cabai Hijau</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 90.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_004.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Bandeng Goreng Bu Lely</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 80.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_004.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Pindang Bandeng Bu Lely</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 85.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Ayam Bumbu Gohiong</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 40.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="product product__no-border border-right">
                                <div class="product__inner overflow-hidden px-3 px-md-4d875">
                                    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="../shop/single-product-v2.html" class="d-block"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html">GKY Sunter</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Durian Monthong Palu 500gr</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">GKY Sunter</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 85.000,-</span>
                                            </div>
                                            <!-- <div class="product__rating d-flex align-items-center font-size-2">
                                                <div class="text-warning mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                <div class="">(3,714)</div>
                                            </div> -->
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="../shop/single-product-v2.html" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="../shop/single-product-v2.html" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="space-bottom-3">
        <div class="container">
            <header class="border-bottom d-md-flex justify-content-between align-items-center mb-8 pb-4d75">
                <h2 class="font-size-7 mb-3 mb-md-0">Toko</h2>
                <a href="../shop/v2.html" class="h-primary d-block">View All <i class="glyph-icon flaticon-next"></i></a>
            </header>
            <div class="js-slick-carousel u-slick products"
                data-pagi-classes="text-center u-slick__pagination mt-8"
                data-slides-show="3"
                data-responsive='[{
                   "breakpoint": 1199,
                   "settings": {
                     "slidesToShow": 2
                   }
                }, {
                   "breakpoint": 768,
                   "settings": {
                     "slidesToShow": 1
                   }
                }, {
                   "breakpoint": 554,
                   "settings": {
                     "slidesToShow": 1
                   }
                }]'>
                <div class="product product__card product__card-v2 border-right">
                    <div class="media p-3 p-md-4d875">
                        <a href="../shop/single-product-v2.html" class="d-block" tabindex="0"><img src="../../assets/img/images/TEMAN SORE_001.png" width="120px" alt="image-description"></a>
                        <div class="media-body ml-4d875">
                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html" tabindex="0">Teman Sore</a></div>
                            <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="../shop/single-product-v2.html" tabindex="0">Wadah GRII Pusat Kebaktian Umum Sore</a></h2>
                            <div class="font-size-2 mb-1 text-truncate"><a href="../others/authors-single.html" class="text-gray-700" tabindex="0">@griipusat.sore</a></div>
                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product product__card product__card-v2 border-right">
                    <div class="media p-3 p-md-4d875">
                        <a href="../shop/single-product-v2.html" class="d-block" tabindex="0"><img src="../../assets/img/images/GKY Sunter Berbagi Info Usaha - Edisi 1_001.png" width="120px" alt="image-description"></a>
                        <div class="media-body ml-4d875">
                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html" tabindex="0">GKY Sunter</a></div>
                            <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="../shop/single-product-v2.html" tabindex="0">Info Kuliner Jemaan GKY Sunter Edisi 1</a></h2>
                            <div class="font-size-2 mb-1 text-truncate"><a href="../others/authors-single.html" class="text-gray-700" tabindex="0">GKY Sunter</a></div>
                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product product__card product__card-v2 border-right">
                    <div class="media p-3 p-md-4d875">
                        <a href="../shop/single-product-v2.html" class="d-block" tabindex="0"><img src="../../assets/img/images/PENDAFTARAN KATALOG(FINAL)_001.png" width="120px" alt="image-description"></a>
                        <div class="media-body ml-4d875">
                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="../shop/single-product-v2.html" tabindex="0">Jembatan</a></div>
                            <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="../shop/single-product-v2.html" tabindex="0">Jejaring Menyebar Bantuan dan Informasi</a></h2>
                            <div class="font-size-2 mb-1 text-truncate"><a href="../others/authors-single.html" class="text-gray-700" tabindex="0">GRII Pusat</a></div>
                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="site-features border-top space-1d625">
        <div class="container">
            <ul class="list-unstyled my-0 list-features d-flex align-items-center justify-content-xl-between overflow-auto overflow-xl-visible">
                <li class="flex-shrink-0 flex-xl-shrink-1 list-feature">
                    <div class="media">
                        <div class="feature__icon font-size-14 text-primary-green text-lh-xs">
                            <i class="glyph-icon flaticon-delivery"></i>
                        </div>
                        <div class="media-body ml-4">
                            <h4 class="feature__title font-size-3">COD Delivery</h4>
                            <p class="feature__subtitle m-0">Metode Pengiriman COD</p>
                        </div>
                    </div>
                </li>
                <li class="flex-shrink-0 flex-xl-shrink-1 separator mx-4 mx-xl-0 border-left h-fixed-80" aria-hidden="true" role="presentation"></li>
                <li class="flex-shrink-0 flex-xl-shrink-1 list-feature">
                    <div class="media">
                        <div class="feature__icon font-size-14 text-primary-green text-lh-xs">
                            <i class="glyph-icon flaticon-credit"></i>
                        </div>
                        <div class="media-body ml-4">
                            <h4 class="feature__title font-size-3">Secure Payment</h4>
                            <p class="feature__subtitle m-0">Transfer Langsung Ke toko / Cash</p>
                        </div>
                    </div>
                </li>
                <li class="flex-shrink-0 flex-xl-shrink-1 separator mx-4 mx-xl-0 border-left h-fixed-80" aria-hidden="true" role="presentation"></li>
                <li class="flex-shrink-0 flex-xl-shrink-1 list-feature">
                    <div class="media">
                        <div class="feature__icon font-size-14 text-primary-green text-lh-xs">
                            <i class="glyph-icon flaticon-warranty"></i>
                        </div>
                        <div class="media-body ml-4">
                            <h4 class="feature__title font-size-3">Terpusat</h4>
                            <p class="feature__subtitle m-0">Pusat Info Kuliner & Produk Jemaat</p>
                        </div>
                    </div>
                </li>
                <li class="flex-shrink-0 flex-xl-shrink-1 separator mx-4 mx-xl-0 border-left h-fixed-80" aria-hidden="true" role="presentation"></li>
                <li class="flex-shrink-0 flex-xl-shrink-1 list-feature">
                    <div class="media">
                        <div class="feature__icon font-size-14 text-primary-green text-lh-xs">
                            <i class="glyph-icon flaticon-help"></i>
                        </div>
                        <div class="media-body ml-4">
                            <h4 class="feature__title font-size-3">24/7 Support</h4>
                            <p class="feature__subtitle m-0">Support 24/7 Jam</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- ===== END MAIN CONTENT ===== -->
<?php include "top/footer.php";?>