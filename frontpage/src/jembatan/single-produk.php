<?php include "top/header.php"; ?>


    <!-- ====== MAIN CONTENT ====== -->
    <div class="page-header border-bottom">
        <div class="container">
            <div class="d-md-flex justify-content-between align-items-center py-4">
                <h1 class="page-title font-size-3 font-weight-medium mb-md-0 text-lh-lg">Acak Adut Kitchen - Babi Kecap + Kentang dan Nasi</h1>
                <nav class="woocommerce-breadcrumb font-size-2">
                    <a href="index.php" class="h-primary">Home</a>
                    <span class="breadcrumb-separator mx-1"><i class="fas fa-angle-right"></i></span>
                    <a href="#" class="h-primary">Teman Sore</a>
                    <span class="breadcrumb-separator mx-1"><i class="fas fa-angle-right"></i></span>Acak Adut Kitchen
                </nav>
            </div>
        </div>
    </div>
    <div id="primary" class="content-area">
        <main id="main" class="site-main ">
            <div class="product">
                <div class="container mb-6">
                    <div class="row">
                        <div class="col-md-4 col-lg-3 woocommerce-product-gallery woocommerce-product-gallery--with-images images">
                            <figure class="woocommerce-product-gallery__wrapper pt-8 mb-0">
                                <div class="js-slick-carousel u-slick"
                                data-pagi-classes="text-center u-slick__pagination my-4">
                                    <div class="js-slide">
                                        <img src="../../assets/img/images/TEMAN SORE_005.png" alt="Image Description" class="mx-auto img-fluid">
                                    </div>
                                    <div class="js-slide">
                                        <img src="../../assets/img/images/TEMAN SORE_005.png" alt="Image Description" class="mx-auto img-fluid">
                                    </div>
                                    <div class="js-slide">
                                        <img src="../../assets/img/images/TEMAN SORE_005.png" alt="Image Description" class="mx-auto img-fluid">
                                    </div>
                                </div>
                            </figure>
                        </div>
                        <div class="col-md-8 col-lg-5 pl-0 summary entry-summary">
                            <div class="space-top-2 pl-4 pl-wd-6 px-wd-7 pb-5">
                                <h1 class="product_title entry-title font-size-7 mb-3">Babi Kecap + Kentang dan Nasi</h1>
                                <div class="font-size-2 mb-4">
                                    <span class="text-yellow-darker">
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                    </span>
                                    <span class="ml-3">(3,714)</span>
                                    <span class="ml-3 font-weight-medium">By Acak Adut Kitchen</span>
                                    <span class="ml-2 text-gray-600">Teman Sore</span>
                                </div>
                                <div class="woocommerce-product-details__short-description font-size-2 mb-5">
                                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat.</p>
                                    <p class="mb-0">*The multi-million copy bestseller*</p>
                                    <p class="mb-0">Soon to be a major film</p>
                                    <p class="mb-4">A Number One New York Times Bestseller</p>
                                    <p class="mb-0">'Painfully beautiful' New York Times</p>
                                    <p class="mb-0">'Unforgettable . . . as engrossing as it is moving' Daily Mail</p>
                                    <p class="mb-0">'A rare achievement' The Times</p>
                                    <p>'I can't even express how much I love this book!' Reese Witherspoon</p>
                                </div>

                                <ul class="list-unstyled my-0 list-features d-xl-flex align-items-center">
                                    <li class="list-feature mb-6 mb-xl-0 mr-xl-4 mr-wd-6">
                                        <div class="media">
                                            <div class="feature__icon font-size-46 text-primary text-lh-xs">
                                                <i class="glyph-icon flaticon-delivery"></i>
                                            </div>
                                            <div class="media-body ml-4">
                                                <h6 class="feature__title">Metode Pengiriman</h6>
                                                <p class="feature__subtitle m-0">COD / Gojek</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-feature mb-6 mb-xl-0">
                                        <div class="media">
                                            <div class="feature__icon font-size-46 text-primary text-lh-xs">
                                                <i class="glyph-icon flaticon-credit"></i>
                                            </div>
                                            <div class="media-body ml-4">
                                                <h6 class="feature__title">Metode Pembayaran</h6>
                                                <p class="feature__subtitle m-0">Transfer / Cash</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="border mt-md-8">
                                <div class="bg-white-100 py-4 px-5">
                                    <p class="price font-size-22 font-weight-medium mb-0">
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">Rp. 50.000
                                        </span>
                                    </p>
                                </div>
                                <div class="py-4 px-5">
                                    <form class="cart mb-4" method="post" enctype="multipart/form-data">
                                        <label class="form-label font-size-2 font-weight-medium">Quantity</label>
                                        <div class="quantity mb-4 d-flex align-items-center">
                                            <!-- Quantity -->
                                            <div class="border px-3 w-100">
                                                <div class="js-quantity">
                                                    <div class="d-flex align-items-center">

                                                        <label class="screen-reader-text sr-only">Quantity</label>
                                                        <a class="js-minus text-dark" href="javascript:;">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="1px">
                                                                <path fill-rule="evenodd" fill="rgb(22, 22, 25)" d="M-0.000,-0.000 L10.000,-0.000 L10.000,1.000 L-0.000,1.000 L-0.000,-0.000 Z" />
                                                            </svg>
                                                        </a>
                                                        <input type="number" class="input-text qty text js-result form-control text-center border-0" step="1" min="1" max="100" name="quantity" value="1" title="Qty">
                                                        <a class="js-plus text-dark" href="javascript:;">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="10px">
                                                                <path fill-rule="evenodd" fill="rgb(22, 22, 25)" d="M10.000,5.000 L6.000,5.000 L6.000,10.000 L5.000,10.000 L5.000,5.000 L-0.000,5.000 L-0.000,4.000 L5.000,4.000 L5.000,-0.000 L6.000,-0.000 L6.000,4.000 L10.000,4.000 L10.000,5.000 Z" />
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Quantity -->
                                        </div>

                                        <button type="submit" name="add-to-cart" value="7145" class="btn btn-block btn-dark border-0 rounded-0 p-3 single_add_to_cart_button button alt">Add to cart</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <section class="space-bottom-3">
                    <div class="container">
                        <header class="mb-5 d-md-flex justify-content-between align-items-center">
                            <h2 class="font-size-7 mb-3 mb-md-0">Menu / Produk Lainnya</h2>
                        </header>

                         <div class="js-slick-carousel products no-gutters border-top border-left border-right"
                            data-arrows-classes="u-slick__arrow u-slick__arrow-centered--y"
                            data-arrow-left-classes="fas fa-chevron-left u-slick__arrow-inner u-slick__arrow-inner--left ml-lg-n10"
                            data-arrow-right-classes="fas fa-chevron-right u-slick__arrow-inner u-slick__arrow-inner--right mr-lg-n10"
                            data-slides-show="5"
                            data-responsive='[{
                               "breakpoint": 1500,
                               "settings": {
                                 "slidesToShow": 4
                               }
                            },{
                               "breakpoint": 1199,
                               "settings": {
                                 "slidesToShow": 3
                               }
                            }, {
                               "breakpoint": 992,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }, {
                               "breakpoint": 554,
                               "settings": {
                                 "slidesToShow": 2
                               }
                            }]'>
                            <div class="product">
                                <div class="product__inner overflow-hidden p-3 p-md-4d875">
                                <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                                </div>
                            </div>

                            <div class="product">
                                <div class="product__inner overflow-hidden p-3 p-md-4d875">
                                <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </section>
            </div>
        </main>
    </div>
    <!-- ====== END MAIN CONTENT ====== -->
    <?php include "top/footer.php";?>
