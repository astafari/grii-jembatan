<?php include "top/header.php"; ?>

    <!-- ====== MAIN CONTENT ===== -->
    <main id="content">
        <div class="space-bottom-2 space-bottom-lg-3">
            <div class="pb-lg-1">
                <div class="page-header border-bottom">
                    <div class="container">
                        <div class="d-md-flex justify-content-between align-items-center py-4">
                            <h1 class="page-title font-size-3 font-weight-medium m-0 text-lh-lg">Acak Adut Kitchen</h1>
                            <nav class="woocommerce-breadcrumb font-size-2">
                                <a href="index.php" class="h-primary">Home</a>
                                <span class="breadcrumb-separator mx-1"><i class="fas fa-angle-right"></i></span>
                                <span>Teman Sore</span>
                                <span class="breadcrumb-separator mx-1"><i class="fas fa-angle-right"></i></span>
                                <span>Acak Adut Kitchen</span>                                
                            </nav>
                        </div>
                    </div>
                </div>

                <section class="space-bottom-2 space-bottom-lg-3">
                    <div class="bg-gray-200 space-bottom-2 space-bottom-md-0">
                        <div class="container space-top-2 space-top-wd-3 px-3">
                            <div class="row">
                                <div class="col-lg-4 col-wd-3 d-flex">
                                    <img class="img-fluid mb-5 mb-lg-0 mt-auto" src="../../assets/img/images/TEMAN SORE_005.png" alt="Image-Description">
                                </div>
                                <div class="col-lg-8 col-wd-9">
                                    <div class="mb-8">
                                        <h6 class="font-size-7 ont-weight-medium mt-2 mb-3 pb-1">
                                            Acak Adut Kitchen
                                        </h6>
                                        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <header class="mb-5">
                        <h2 class="font-size-7 mb-0">Acak Adut Kitchen - Menu</h2>
                    </header>
                    <ul class="js-slick-carousel products list-unstyled my-0 border-right products border-top border-left"
                        data-arrows-classes="d-none d-lg-block u-slick__arrow u-slick__arrow-centered--y"
                        data-arrow-left-classes="fas flaticon-back u-slick__arrow-inner u-slick__arrow-inner--left ml-lg-n10" data-arrow-right-classes="fas flaticon-next u-slick__arrow-inner u-slick__arrow-inner--right mr-lg-n10"
                        data-slides-show="4"
                        data-responsive='[{
                           "breakpoint": 992,
                           "settings": {
                             "slidesToShow": 2
                           }
                        }, {
                           "breakpoint": 768,
                           "settings": {
                             "slidesToShow": 1
                           }
                        }, {
                           "breakpoint": 554,
                           "settings": {
                             "slidesToShow": 1
                           }
                        }]'>
                        <li class="product">
                            <div class="product__inner overflow-hidden p-4d875">
                                 <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product__inner overflow-hidden p-4d875">
                                 <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product__inner overflow-hidden p-4d875">
                                 <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product__inner overflow-hidden p-4d875">
                                 <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link d-block position-relative">
                                        <div class="woocommerce-loop-product__thumbnail">
                                            <a href="single-produk.php" class="d-block"><img src="../../assets/img/images/TEMAN SORE_005.png" class="d-block mx-auto attachment-shop_catalog size-shop_catalog wp-post-image img-fluid" alt="image-description"></a>
                                        </div>
                                        <div class="woocommerce-loop-product__body product__body pt-3 bg-white">
                                            <div class="text-uppercase font-size-1 mb-1 text-truncate"><a href="single-toko.php">Acak Adut Kitchen</a></div>
                                            <h2 class="woocommerce-loop-product__title product__title h6 text-lh-md mb-1 text-height-2 crop-text-2 h-dark"><a href="#">Babi Kecap + Kentang dan Nasi</a></h2>
                                            <div class="font-size-2  mb-1 text-truncate"><a href="others/authors-single.html" class="text-gray-700">Teman Sore</a></div>
                                            <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>Rp. 50.000,-</span>
                                            </div>
                                        </div>
                                        <div class="product__hover d-flex align-items-center">
                                            <a href="single-produk.php" class="text-uppercase text-dark h-dark font-weight-medium mr-auto" data-toggle="tooltip" data-placement="right" title="ADD TO CART">
                                                <span class="product__add-to-cart">ADD TO CART</span>
                                                <span class="product__add-to-cart-icon font-size-4"><i class="flaticon-icon-126515"></i></span>
                                            </a>
                                            <a href="single-produk.php" class="mr-1 h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-switch"></i>
                                            </a>
                                            <a href="single-produk.php" class="h-p-bg btn btn-outline-primary-green border-0">
                                                <i class="flaticon-heart"></i>
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
    <!-- ====== END MAIN CONTENT ===== -->
    <?php include "top/footer.php";?>